# This file defines the AWS infrastructure to run Redis.
#
# We run Redis on ElastiCache.

variable "redis_primary_engine_version" {
  description = "Version of Redis to use in ElastiCache"
  default = "5.0.6"
}

variable "redis_primary_maintenance_window" {
  description = "Maintenance window for primary Redis instance"
  default = "Sun:00:00-Sun:02:00"
}

variable "redis_primary_node_type" {
  description = "ElastiCache node type for primary Redis instance"
  # Small by default to keep costs in check.
  default = "cache.t3.medium"
}

variable "redis_primary_snapshot_window" {
  description = "Time window for taking snapshots of Redis primary"
  default = "07:00-09:00"
}

variable "redis_primary_snapshot_retention_limit" {
  description = "Number of days Redis primary snapshots should be retained for"
  type = number
  default = "1"
}

variable "redis_primary_parameter_group_name" {
  description = "Name of parameter group for Redis primary"
  default = "default.redis5.0"
}

resource "aws_security_group" "redis_primary" {
  name = "gitlab-${var.cluster_name}-redis-primary"
  description = "Security group for primary Redis instance for ${var.cluster_name}"
  vpc_id = var.vpc_id
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

resource "aws_elasticache_cluster" "primary" {
  cluster_id = "gitlab-${var.cluster_name}-primary"
  engine = "redis"
  engine_version = var.redis_primary_engine_version
  maintenance_window = var.redis_primary_maintenance_window
  node_type = var.redis_primary_node_type
  # Must be 1 for Redis.
  num_cache_nodes = 1
  # TODO support custom parameter group?
  parameter_group_name = var.redis_primary_parameter_group_name
  subnet_group_name = var.elasticache_subnet_group_name
  security_group_ids = [
    aws_security_group.redis_primary.id,
  ]
  apply_immediately = false
  snapshot_window = var.redis_primary_snapshot_window
  snapshot_retention_limit = var.redis_primary_snapshot_retention_limit
  availability_zone = var.subnets_private[0].availability_zone
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}
