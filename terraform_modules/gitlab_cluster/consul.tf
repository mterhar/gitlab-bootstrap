resource "aws_iam_role" "consul" {
  name = "gitlab-${var.cluster_name}-consul"
  description = "Role for GitLab consul EC2 instances for ${var.cluster_name} cluster"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

resource "aws_iam_instance_profile" "consul" {
  name = aws_iam_role.consul.name
  role = aws_iam_role.consul.name
}

resource "aws_iam_role_policy_attachment" "consul_ansible" {
  role = aws_iam_role.consul.name
  policy_arn = aws_iam_policy.ansible.arn
}

# Create a dedicated security group for Consul network flows.
resource "aws_security_group" "consul" {
  name = "gitlab-${var.cluster_name}-consul"
  description = "Security group for Consul EC2 instances for ${var.cluster_name} cluster"
  vpc_id = var.vpc_id

  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

# We spin up a cluster of dedicated instances to run Consul
resource "aws_instance" "consul" {
  count = var.consul_instance_count
  instance_type = var.consul_instance_type
  ami = var.instance_ami
  iam_instance_profile = aws_iam_instance_profile.consul.name
  associate_public_ip_address = false
  availability_zone = var.subnets_private[count.index].availability_zone
  subnet_id = var.subnets_private[count.index].id
  vpc_security_group_ids = [
    aws_security_group.consul.id,
  ]
  user_data = templatefile("${path.module}/gitlab_user_data.sh", {
    ansible_variables_s3_url = local.instance_id_variables_s3_url
    gitlab_cluster_state_s3_url = local.cluster_state_s3_url,
    gitlab_role = "consul",
    ssh_authorized_keys = var.ssh_authorized_keys,
    ignore_cluster_ready_semaphore = "false"
  })
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "consul"
  }
  volume_tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "consul"
  }
}

locals {
  consul_hostnames = [for i in range(length(aws_instance.consul)) : "consul-${i}.${aws_acm_certificate.main.domain_name}"]
}

# Each consul instance receives a DNS record for that instance.
resource "aws_route53_record" "consul" {
  count = length(aws_instance.consul)
  zone_id = aws_route53_zone.cluster.id
  name = "consul-${count.index}.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = "60"
  records = [aws_instance.consul[count.index].private_ip]
}

# We also expose a record for all Consul instances, to allow
# for easy querying.
resource "aws_route53_record" "consul_all" {
  zone_id = aws_route53_zone.cluster.id
  name = "consul-all.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = 60
  records = aws_instance.consul[*].private_ip
}

# For each Consul EC2 instance, we write an S3 object containing
# Ansible variables to help bootstrap the instance. The object
# is named after the instance ID. This ensures that each instance
# gets state intended for it and only it.
resource "aws_s3_bucket_object" "consul_ansible_variables" {
  count = length(aws_instance.consul)
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/ansible-instance-variables/${aws_instance.consul[count.index].id}.json"
  content = jsonencode({
    consul_hostnames: local.consul_hostnames
    gitlab_debian_package_s3_url: local.gitlab_debian_package_s3_url
    gitlab_debian_package_sha256: var.gitlab_debian_package.sha256
    cluster_state_s3_url: local.cluster_state_s3_url
    skip_gitlab_secrets_download: false
  })
  content_type = "application/json"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}
