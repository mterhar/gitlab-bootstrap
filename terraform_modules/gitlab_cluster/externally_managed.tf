# Resources in this module are "external" to us and aren't managed
# by us. We define them as our resources rather than use a remote state
# reference in order to minimize bootstrap dependencies.
#
# The resources are defined as resources to facilitate strong
# referencing, so Terraform dependencies, validations, etc work.
#
# These resources are automatically imported into state during
# initial bootstrap.
#
# We initially tried to import all external resources - such as
# the bootstrap and cluster state S3 buckets - into our state. However,
# even with lifecycle rules preventing destruction, this prevented
# `terraform destroy` from working. So instead we rely on regular
# variables and locals for externally managed resources.

locals {
  bootstrap_bucket_arn = "arn:aws:s3:::${var.bootstrap_bucket}"
  cluster_state_bucket_arn = "arn:aws:s3:::${var.cluster_state_bucket}"
}

data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
    actions = ["sts:AssumeRole"]
  }
}

# The bootstrap IAM role and initial policy is created by the
# Python bootstrapper tool.
#
# We define them redundantly here so Terraform can have full
# control of the role going forward.

resource "aws_iam_role" "bootstrap" {
  name = "gitlab-${var.cluster_name}-bootstrap"
  description = "Used to bootstrap the ${var.cluster_name} GitLab cluster"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "gitlab-bootstrap"
    GitLabCluster = var.cluster_name
  }
}

resource "aws_iam_role_policy" "bootstrap_bootstrap" {
  name = "bootstrap"
  role = aws_iam_role.bootstrap.id
  policy = var.bootstrap_role_initial_policy
}
