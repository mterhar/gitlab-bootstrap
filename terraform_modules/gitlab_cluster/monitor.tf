# This file defines the AWS resources to run the monitoring node
# for a GitLab cluster. The monitoring node runs Prometheus and
# Grafana.

variable "monitor_instance_type" {
  description = "EC2 instance type to use for monitoring node"
  default = "c5.large"
}

resource "aws_iam_role" "monitor" {
  name = "gitlab-${var.cluster_name}-monitor"
  description = "Role for GitLab monitoring EC2 instance for ${var.cluster_name}"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "monitor"
  }
}

resource "aws_iam_instance_profile" "monitor" {
  name = aws_iam_role.monitor.name
  role = aws_iam_role.monitor.name
}

resource "aws_iam_role_policy_attachment" "monitor_ansible" {
  role = aws_iam_role.monitor.name
  policy_arn = aws_iam_policy.ansible.arn
}

# Create dedicated secrity group for this EC2 instance.
resource "aws_security_group" "monitor" {
  name = "gitlab-${var.cluster_name}-monitor"
  description = "Security group for Monitor EC2 instance for ${var.cluster_name}"
  vpc_id = var.vpc_id
  # Allow incoming connections from load balancer.
  ingress {
    description = "Allow Prometheus connections from load balancer"
    protocol = "tcp"
    from_port = 9090
    to_port = 9090
    cidr_blocks = [for id in var.subnets_private[*].id : local.private_subnet_cidr_blocks[id]]
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "monitor"
  }
}

resource "aws_instance" "monitor" {
  instance_type = var.monitor_instance_type
  ami = var.instance_ami
  iam_instance_profile = aws_iam_instance_profile.monitor.name
  associate_public_ip_address = false
  availability_zone = var.subnets_private[0].availability_zone
  subnet_id = var.subnets_private[0].id
  vpc_security_group_ids = [
    aws_security_group.monitor.id,
  ]
  user_data = templatefile("${path.module}/gitlab_user_data.sh", {
    ansible_variables_s3_url = local.instance_id_variables_s3_url
    ssh_authorized_keys = var.ssh_authorized_keys,
    gitlab_cluster_state_s3_url = "s3://${var.cluster_state_bucket}/${local.cluster_state_key_prefix}",
    gitlab_role = "monitor",
    ignore_cluster_ready_semaphore = "false"
  })
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "monitor"
  }
  volume_tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "monitor"
  }
}

resource "random_password" "grafana_admin_password" {
  length = 24
}

resource "aws_s3_bucket_object" "monitor_ansible_variables" {
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/ansible-instance-variables/${aws_instance.monitor.id}.json"
  content = jsonencode({
    gitlab_debian_package_s3_url: local.gitlab_debian_package_s3_url
    gitlab_debian_package_sha256: var.gitlab_debian_package.sha256

    cluster_state_s3_url = local.cluster_state_s3_url
    skip_gitlab_secrets_download = false

    consul_hostnames = local.consul_hostnames

    grafana_admin_password = random_password.grafana_admin_password.result
  })
  content_type = "application/json"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "monitor"
  }
}

# Define a hostname for this EC2 instance, for convenience.
resource "aws_route53_record" "monitor" {
  zone_id = aws_route53_zone.cluster.id
  name = "monitor.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = "60"
  records = [aws_instance.monitor.private_ip]
}

# Set up a Prometheus endpoint on the load balancer. This requires
# a hostname with its own certificate.
resource "aws_route53_record" "prometheus" {
  zone_id = aws_route53_zone.cluster.zone_id
  name = "prometheus.${aws_route53_zone.cluster.name}"
  type = "CNAME"
  ttl = "60"
  records = [aws_lb.app_server.dns_name]
}

resource "aws_acm_certificate" "prometheus" {
  domain_name = aws_route53_record.prometheus.name
  validation_method = "DNS"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "monitor"
  }
}

resource "aws_route53_record" "prometheus_validation" {
  name = aws_acm_certificate.prometheus.domain_validation_options.0.resource_record_name
  type = aws_acm_certificate.prometheus.domain_validation_options.0.resource_record_type
  zone_id = aws_route53_zone.cluster.zone_id
  ttl = 60
  records = [
    aws_acm_certificate.prometheus.domain_validation_options.0.resource_record_value
  ]
}

resource "aws_acm_certificate_validation" "prometheus" {
  certificate_arn = aws_acm_certificate.prometheus.arn
  validation_record_fqdns = [
    aws_route53_record.prometheus_validation.fqdn,
  ]
}

resource "aws_lb_target_group" "prometheus" {
  name = "gitlab-${var.cluster_name}-prometheus"
  port = 9090
  protocol = "TCP"
  vpc_id = var.vpc_id
  deregistration_delay = 30
  target_type = "instance"
  health_check {
    protocol = "TCP"
    interval = 10
    healthy_threshold = 3
    unhealthy_threshold = 3
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "monitor"
  }
}

resource "aws_lb_target_group_attachment" "prometheus" {
  target_group_arn = aws_lb_target_group.prometheus.arn
  target_id = aws_instance.monitor.id
  port = 9090
}

resource "aws_lb_listener" "prometheus_https" {
  load_balancer_arn = aws_lb.app_server.arn
  port = 9090
  protocol = "TLS"
  certificate_arn = aws_acm_certificate.prometheus.arn
  ssl_policy = "ELBSecurityPolicy-FS-1-2-2019-08"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.prometheus.arn
  }
}
