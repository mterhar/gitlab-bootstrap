variable "cluster_name" {
  description = "Name of GitLab cluster (for tagging)"
}

variable "identifier" {
  description = "Name of database (suffix after gitlab-{cluster_name}-)"
}

variable "db_name" {
  description = "Name of database to create"
}

variable "availability_zone" {
  description = "Availability zone to place the database into"
}

variable "subnet_group_name" {
  description = "Name of RDS subnet to place instance into"
}

variable "security_group_ids" {
  description = "List of VPC security group IDs to attach to database"
  type = list(string)
}

variable "allocated_storage" {
  description = "Number of gigabytes to initially allocate toward storage"
  type = number
  # 6 appears to be the smallest we can go.
  default = 6
}

variable "auto_minor_version_upgrade" {
  description = "Whether the database is upgraded between minor versions automatically"
  type = bool
  default = true
}

variable "backup_retention_period" {
  description = "Days for database to retain backups for"
  type = number
  default = 3
}

variable "backup_window" {
  description = "Backup window for database"
  default = "03:00-05:00"
}

variable "deletion_protection" {
  description = "Deletion protection for database"
  type = bool
  # TODO consider changing this
  default = false
}

variable "engine_version" {
  description = "Engine version for database"
  default = "11.7"
}

variable "instance_class" {
  description = "RDS instance class for database"
  # We are conservative by default to keep costs in check.
  default = "db.m5.xlarge"
}

variable "maintenance_window" {
  description = "Maintenance window for database"
  default = "Sun:00:00-Sun:02:00"
}

variable "max_allocated_storage" {
  description = "Max allocated storage for database"
  type = number
  # Disabled by default to prevent storage run-ups.
  default = 0
}

variable "multi_az" {
  description = "Whether the database should span multiple availability zones"
  type = bool
  # Default is false to keep costs in check.
  default = false
}

variable "storage_type" {
  description = "Storage type for database"
  # gp2 by default because it is cheaper.
  default = "gp2"
}

variable "username" {
  description = "Default database username"
  default = "gitlab"
}

resource "random_password" "password" {
  length = 24
  # We have to take out some special characters in default list
  # because they are invalid in SQL.
  override_special = "!#%&*()-_=+:?"
}

resource "aws_db_instance" "db" {
  allocated_storage = var.allocated_storage
  # We want to manage the major version of the DB with GitLab upgrades.
  # Do not allow major version upgrades.
  allow_major_version_upgrade = false
  # We want database modifications to be explicitly under our
  # control.
  apply_immediately = false
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  availability_zone = var.availability_zone
  backup_retention_period = var.backup_retention_period
  backup_window = var.backup_window
  copy_tags_to_snapshot = true
  db_subnet_group_name = var.subnet_group_name
  delete_automated_backups = true
  deletion_protection = var.deletion_protection
  # TODO support enabled_cloudwatch_logs_exports
  engine = "postgres"
  engine_version = var.engine_version
  iam_database_authentication_enabled = false
  identifier = "gitlab-${var.cluster_name}-${var.identifier}"
  instance_class = var.instance_class
  # TODO iops
  maintenance_window = var.maintenance_window
  max_allocated_storage = var.max_allocated_storage
  # TODO monitoring_interval and monitoring_role_arn
  multi_az = var.multi_az
  name = var.db_name
  # TODO option_group_name, parameter_group_name
  password = random_password.password.result
  publicly_accessible = false
  # TODO support performing final snapshots
  skip_final_snapshot = true
  # TODO support storage encryption
  storage_encrypted = false
  storage_type = var.storage_type
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
  username = "gitlab"
  vpc_security_group_ids = var.security_group_ids
  # TODO support performance insights?
}

output "address" {
  value = aws_db_instance.db.address
}

output "port" {
  value = aws_db_instance.db.port
}

output "username" {
  value = aws_db_instance.db.username
}

output "password" {
  value = aws_db_instance.db.password
  sensitive = true
}

output "db_name" {
  value = aws_db_instance.db.name
}