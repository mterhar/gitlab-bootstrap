# This file defines the AWS infrastructure to run PostgreSQL.
#
# We run PostgreSQL on RDS. Due to varying cluster availability
# and scaling requirements, we support for config knobs to tweak
# behavior.


resource "aws_security_group" "postgres_primary" {
  name = "gitlab-${var.cluster_name}-postgres-primary"
  description = "Security group for primary database for ${var.cluster_name}"
  vpc_id = var.vpc_id

  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

# TODO define variables to override defaults to more reasonable values.
module "primary_database" {
  source = "./postgres_database"
  cluster_name = var.cluster_name
  identifier = "primary"
  db_name = "gitlab"
  availability_zone = var.subnets_private[0].availability_zone
  subnet_group_name = var.db_subnet_group_name
  security_group_ids = [
    aws_security_group.postgres_primary.id,
  ]
  allocated_storage = 32
}

locals {
  postgres_primary_address = module.primary_database.address
  postgres_primary_port = module.primary_database.port
  postgres_primary_username = module.primary_database.username
  postgres_primary_password = module.primary_database.password
  postgres_primary_db_name = module.primary_database.db_name
}
