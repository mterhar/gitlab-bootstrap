# Each cluster has a single "admin" node EC2 instance that exists
# for coordinating cluster-wide activity.

resource "aws_iam_role" "admin" {
  name = "gitlab-${var.cluster_name}-admin"
  description = "Role for GitLab cluster admin EC2 instance for ${var.cluster_name}"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "admin"
  }
}

resource "aws_iam_instance_profile" "admin" {
  name = aws_iam_role.admin.name
  role = aws_iam_role.admin.name
}

resource "aws_iam_role_policy_attachment" "admin_ansible" {
  role = aws_iam_role.admin.name
  policy_arn = aws_iam_policy.ansible.arn
}

resource "aws_iam_role_policy_attachment" "admin_elasticsearch" {
  role = aws_iam_role.admin.name
  policy_arn = aws_iam_policy.elasticsearch_domain_access.arn
}

data "aws_iam_policy_document" "admin" {
  # Allow the admin instance to write a semaphore file and secrets data.
  statement {
    effect = "Allow"
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "${local.cluster_state_arn_prefix}/ansible/cluster_ready_semaphore",
      # TODO store secrets securely.
      "${local.cluster_state_arn_prefix}/ansible/gitlab-secrets.json",
    ]
  }
}

resource "aws_iam_role_policy" "admin_admin" {
  role = aws_iam_role.admin.name
  policy = data.aws_iam_policy_document.admin.json
}

resource "aws_security_group" "admin" {
  name = "gitlab-${var.cluster_name}-admin"
  description = "Security group for admin EC2 instances for ${var.cluster_name}"
  vpc_id = var.vpc_id
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "admin"
  }
}

resource "aws_instance" "admin" {
  instance_type = "c5.2xlarge"
  ami = var.instance_ami
  iam_instance_profile = aws_iam_instance_profile.admin.name
  associate_public_ip_address = false
  availability_zone = var.subnets_private[0].availability_zone
  subnet_id = var.subnets_private[0].id
  vpc_security_group_ids = [
    aws_security_group.admin.id,
  ]
  root_block_device {
    volume_size = 32
  }
  user_data = templatefile("${path.module}/gitlab_user_data.sh", {
    gitlab_cluster_state_s3_url = local.cluster_state_s3_url
    ansible_variables_s3_url = local.instance_id_variables_s3_url
    gitlab_role = "admin"
    ssh_authorized_keys = var.ssh_authorized_keys
    ignore_cluster_ready_semaphore = "true"
  })
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "admin"
  }
  volume_tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "admin"
  }
}

resource "aws_s3_bucket_object" "admin_ansible_variables" {
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/ansible-instance-variables/${aws_instance.admin.id}.json"
  content = jsonencode(merge(
    local.praefect_common_variables, {
    gitlab_debian_package_s3_url: local.gitlab_debian_package_s3_url
    gitlab_debian_package_sha256: var.gitlab_debian_package.sha256

    cluster_hostname = aws_acm_certificate.main.domain_name
    cluster_url = "https://${aws_acm_certificate.main.domain_name}"

    cluster_state_s3_url = local.cluster_state_s3_url
    skip_gitlab_secrets_download = true

    consul_hostnames = local.consul_hostnames

    initial_root_password: random_password.initial_root_password.result

    # The admin node essentially receives the state of the world
    # for the cluster.
    postgres_primary_address: local.postgres_primary_address
    postgres_primary_port: local.postgres_primary_port
    postgres_primary_username: local.postgres_primary_username
    postgres_primary_password: local.postgres_primary_password
    postgres_primary_db_name: local.postgres_primary_db_name

    redis_host: aws_elasticache_cluster.primary.cache_nodes[0].address
    redis_port: aws_elasticache_cluster.primary.cache_nodes[0].port

    elasticsearch_endpoint = aws_elasticsearch_domain.main.endpoint
  }))
  content_type = "application/json"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "admin"
  }
}

# Write an S3 object containing the IP address of the admin instance.
# This allows the admin node to be discovered more easily.
resource "aws_s3_bucket_object" "admin_ip" {
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/admin_ip.txt"
  content = aws_instance.admin.private_ip
  content_type = "text/plain"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "admin"
  }
}

# Define a hostname for this EC2 instance, for convenience.
resource "aws_route53_record" "admin" {
  zone_id = aws_route53_zone.cluster.id
  name = "admin.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = "60"
  records = [aws_instance.admin.private_ip]
}
