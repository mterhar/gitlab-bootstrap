# We define an additional IAM policy on the bootstrap role to allow
# the bootstrap tool to perform required Terraform actions necessary
# to bootstrap this cluster.
#
# The attachment of this policy is the first thing done when running
# Terraform, as the grant of extra permissions is necessary to do
# anything meaningful with Terraform.
#
# Note that we CANNOT reference resources "strongly" here, as that
# causes Terraform to pull them in as dependencies, which breaks
# the initial grant scope. So we have to use strings for all ARNs.
data "aws_iam_policy_document" "bootstrap_terraform" {
  # Allow reading and writing S3 objects for our cluster state.
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject*",
      "s3:PutObject*",
      "s3:DeleteObject*",
    ]
    resources = [
      "${local.cluster_state_arn_prefix}/*",
    ]
  }
  # Allow querying object versions in cluster state bucket.
  # Terraform needs this in order to manage S3 objects.
  statement {
    effect = "Allow"
    actions = [
      "s3:ListBucketVersions",
    ]
    resources = [
      "arn:aws:s3:::${var.cluster_state_bucket}",
    ]
  }
  # Allow modification of IAM roles associated with this cluster.
  statement {
    effect = "Allow"
    actions = [
      "iam:AttachRolePolicy",
      "iam:CreateRole",
      "iam:DeleteRolePolicy",
      "iam:DeleteRole",
      "iam:DetachRolePolicy",
      "iam:GetRolePolicy",
      "iam:GetRole",
      "iam:ListAttachedRolePolicies",
      "iam:ListInstanceProfilesForRole",
      "iam:PassRole",
      "iam:PutRolePolicy",
      "iam:TagRole",
    ]
    resources = [
      "${local.iam_role_arn_prefix}-*",
    ]
  }
  # Allow modification of IAM instance profiles associated with this
  # cluster.
  statement {
    effect = "Allow"
    actions = [
      "iam:AddRoleToInstanceProfile",
      "iam:CreateInstanceProfile",
      "iam:DeleteInstanceProfile",
      "iam:GetInstanceProfile",
      "iam:RemoveRoleFromInstanceProfile",
    ]
    resources = [
      "${local.iam_instance_profile_arn_prefix}-*",
    ]
  }
  # Allow modification of IAM policies belonging to this cluster.
  statement {
    effect = "Allow"
    actions = [
      "iam:CreatePolicy",
      "iam:CreatePolicyVersion",
      "iam:DeletePolicy",
      "iam:DeletePolicyVersion",
      "iam:GetPolicy",
      "iam:GetPolicyVersion",
      "iam:ListPolicyVersions",
    ]
    resources = [
      "${local.iam_policy_arn_prefix}-*",
    ]
  }
  # Allow read-only access to EC2 resources where APIs can't be scoped.
  statement {
    effect = "Allow"
    actions = [
      "ec2:DescribeImages",
      "ec2:DescribeInstanceAttribute",
      "ec2:DescribeInstances",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeTags",
      "ec2:DescribeVolumes",
    ]
    resources = ["*"]
  }
  # Allow write access to EC2 actions that can't be scoped.
  statement {
    effect = "Allow"
    actions = [
      "ec2:ModifyInstanceAttribute",
    ]
    resources = ["*"]
  }

  # Allow modification of EC2 instances associated with this cluster.
  # Instances are associated with this cluster if they have an IAM
  # instance profile managed by us. We should only launch instances
  # with such IAM instance profiles.
  statement {
    effect = "Allow"
    actions = [
      "ec2:RunInstances",
      "ec2:StopInstances",
      "ec2:StartInstances",
      "ec2:TerminateInstances",
    ]
    resources = [
      "${local.ec2_instance_arn_prefix}/*"
    ]
    condition {
      test = "StringLike"
      variable = "ec2:InstanceProfile"
      values = [
        "${local.iam_instance_profile_arn_prefix}-*",
      ]
    }
  }
  # ec2:RunInstances also requiress permissions on associated resources.
  # So give blanket permissions for just that API here.
  statement {
    effect = "Allow"
    actions = ["ec2:RunInstances"]
    resources = [
      "arn:aws:ec2:*:*:image/*",
      "arn:aws:ec2:*:*:launch-template/*",
      "arn:aws:ec2:*:*:network-interface/*",
      "arn:aws:ec2:*:*:snapshot/*",
      "arn:aws:ec2:*:*:security-group/*",
      "arn:aws:ec2:*:*:subnet/*",
      "arn:aws:ec2:*:*:volume/*",
    ]
  }
  # Allow tagging EC2 instances.
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateTags",
    ]
    resources = [
      "arn:aws:ec2:*:*:instance/*",
    ]
    condition {
      test = "StringLike"
      variable = "ec2:InstanceProfile"
      values = [
        "${local.iam_instance_profile_arn_prefix}-*",
      ]
    }
  }
  # Allow tagging EC2 volumes. Our abilities to conditionalize this
  # grant are minimal. The best we can do is limit by tag name.
  # That feels like more effort than it is worth.
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateTags",
    ]
    resources = [
      "arn:aws:ec2:*:*:volume/*",
    ]
  }
  # Allow managing security groups. Unfortunately, these actions
  # do not resource conditionalization.
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateSecurityGroup",
      "ec2:DescribeSecurityGroups",
      "ec2:ModifyNetworkInterfaceAttribute",
    ]
    resources = ["*"]
  }
  # Allow tagging security groups.
  statement {
    effect = "Allow"
    actions = ["ec2:CreateTags"]
    resources = [
      "arn:aws:ec2:*:*:security-group/*",
    ]
  }
  # Allow managing our own security groups for actions that allow
  # resource scoping.
  statement {
    effect = "Allow"
    actions = [
      "ec2:AuthorizeSecurityGroupEgress",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:DeleteSecurityGroup",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:RevokeSecurityGroupIngress",
      "ec2:UpdateSecurityGroupRuleDescriptionsEgress",
      "ec2:UpdateSecurityGroupRuleDescriptionsIngress",
    ]
    resources = [
      "${local.security_group_arn_prefix}/*",
    ]
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/Name"
      values = ["gitlab"]
    }
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/GitLabCluster"
      values = [var.cluster_name]
    }
  }
  # Allow creation of EBS volumes.
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateVolume",
    ]
    resources = [
      "arn:aws:ec2:*:*:volume/*",
    ]
  }
  # Allow deletion of our EBS volumes.
  statement {
    effect = "Allow"
    actions = [
      "ec2:DeleteVolume",
    ]
    resources = [
      "arn:aws:ec2:*:*:volume/*",
    ]
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/Name"
      values = ["gitlab"]
    }
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/GitLabCluster"
      values = [var.cluster_name]
    }
  }
  # Grant EBS actions that cannot be scoped.
  statement {
    effect = "Allow"
    actions = [
      "ec2:DescribeVolume*",
      "ec2:ModifyVolume",
      "ec2:ModifyVolumeAttribute",
    ]
    resources = ["*"]
  }
  # Allow EBS volume operations on our EC2 instances.
  statement {
    effect = "Allow"
    actions = [
      "ec2:AttachVolume",
      "ec2:DetachVolume",
    ]
    resources = [
      "arn:aws:ec2:*:*:instance/*",
      "arn:aws:ec2:*:*:volume/*",
    ]
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/Name"
      values = ["gitlab"]
    }
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/GitLabCluster"
      values = [var.cluster_name]
    }
  }
  # Allow modification of RDS databases belonging to this cluster.
  statement {
    effect = "Allow"
    actions = [
      "rds:AddRoleToDBInstance",
      "rds:AddTagsToResource",
      "rds:CreateDBInstance",
      "rds:DeleteDBInstance",
      "rds:ListTagsForResource",
      "rds:ModifyDBInstance",
      "rds:RemoveRoleFromDBInstance",
      "rds:RemoveTagsFromResource"
    ]
    resources = [
      "arn:aws:rds:*:*:db:gitlab-${var.cluster_name}-*",
      "arn:aws:rds:*:*:pg:gitlab-${var.cluster_name}-*",
      # These actions also require permissions on associated resources.
      "arn:aws:rds:*:*:subgrp:${var.db_subnet_group_name}",
    ]
  }
  # Grant RDS actions that can't be scoped.
  statement {
    effect = "Allow"
    actions = [
      "rds:DescribeDBInstances",
    ]
    resources = ["*"]
  }
  # Grant ElastiCache actions that can't be scoped.
  statement {
    effect = "Allow"
    actions = [
      "elasticache:AddTagsToResource",
      "elasticache:CreateCacheCluster",
      "elasticache:DescribeCacheClusters",
      "elasticache:DeleteCacheCluster",
      "elasticache:ListTagsForResource",
      "elasticache:ModifyCacheCluster",
      "elasticache:RemoveTagsFromResource",
    ]
    resources = ["*"]
  }
  # Grant launch template actions that can't be scoped.
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateLaunchTemplate",
      "ec2:DescribeLaunchTemplateVersions",
      "ec2:DescribeLaunchTemplates",
      "ec2:GetLaunchTemplateData",
    ]
    resources = ["*"]
  }
  # Grant launch template actions that can be scoped.
  #
  # Resource IDs are opaque. So we need to use resource tags to
  # conditionalize grant.
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateLaunchTemplateVersion",
      "ec2:DeleteLaunchTemplate",
      "ec2:DeleteLaunchTemplateVersions",
      "ec2:ModifyLaunchTemplate",
    ]
    resources = [
      "arn:aws:ec2:*:*:launch-template/*",
    ]
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/Name"
      values = ["gitlab"]
    }
    condition {
      test = "StringEquals"
      variable = "ec2:ResourceTag/GitLabCluster"
      values = [var.cluster_name]
    }
  }
  # Allow tagging launch templates.
  statement {
    effect = "Allow"
    actions = [
      "ec2:CreateTags",
    ]
    resources = [
      "arn:aws:ec2:*:*:launch-template/*",
    ]
  }
  # Allow managing service linked IAM roles for this cluster.
  statement {
    effect = "Allow"
    actions = [
      "iam:CreateServiceLinkedRole",
      "iam:DeleteServiceLinkedRole",
      "iam:GetRole",
      "iam:GetServiceLinkedRoleDeletionStatus",
      "iam:PassRole",
    ]
    resources = [
      "arn:aws:iam::*:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling",
      "arn:aws:iam::*:role/aws-service-role/es.amazonaws.com/AWSServiceRoleForAmazonElasticsearchService",
    ]
  }
  # Allow managing load balancers for this cluster.
  statement {
    effect = "Allow"
    actions = [
      "elasticloadbalancing:*",
    ]
    resources = [
      "arn:aws:elasticloadbalancing:*:*:listener/app/gitlab-${var.cluster_name}-*",
      "arn:aws:elasticloadbalancing:*:*:listener-rule/app/gitlab-${var.cluster_name}-*",
      "arn:aws:elasticloadbalancing:*:*:listener/net/gitlab-${var.cluster_name}-*",
      "arn:aws:elasticloadbalancing:*:*:listener-rule/net/gitlab-${var.cluster_name}-*",
      "arn:aws:elasticloadbalancing:*:*:loadbalancer/gitlab-${var.cluster_name}-*",
      "arn:aws:elasticloadbalancing:*:*:loadbalancer/app/gitlab-${var.cluster_name}-*",
      "arn:aws:elasticloadbalancing:*:*:loadbalancer/net/gitlab-${var.cluster_name}-*",
      "arn:aws:elasticloadbalancing:*:*:targetgroup/gitlab-${var.cluster_name}-*",
    ]
  }
  # Allow load balancer actions that cannot be scoped.
  statement {
    effect = "Allow"
    actions = [
      "elasticloadbalancing:DescribeListeners",
      "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DescribeLoadBalancerPolicies",
      "elasticloadbalancing:DescribeLoadBalancerPolicyTypes",
      "elasticloadbalancing:DescribeLoadBalancers",
      "elasticloadbalancing:DescribeTags",
      "elasticloadbalancing:DescribeTargetGroupAttributes",
      "elasticloadbalancing:DescribeTargetGroups",
      "elasticloadbalancing:DescribeTargetHealth",
    ]
    resources = ["*"]
  }

  # Allow managing auto scaling groups for this cluster.
  statement {
    effect = "Allow"
    actions = [
      "autoscaling:AttachLoadBalancerTargetGroups",
      "autoscaling:AttachLoadBalancers",
      "autoscaling:CreateAutoScalingGroup",
      "autoscaling:CreateOrUpdateTags",
      "autoscaling:DeleteAutoScalingGroup",
      "autoscaling:DeletePolicy",
      "autoscaling:DeleteTags",
      "autoscaling:DetachLoadBalancerTargetGroups",
      "autoscaling:DetachLoadBalancers",
      "autoscaling:DisableMetricsCollection",
      "autoscaling:EnableMetricsCollection",
      "autoscaling:PutScalingPolicy",
      "autoscaling:UpdateAutoScalingGroup",
    ]
    resources = [
      "arn:aws:autoscaling:*:*:autoScalingGroup:*:autoScalingGroupName/gitlab-${var.cluster_name}-*",
    ]
  }
  # Grant autoscaling actions that cannot be scoped.
  statement {
    effect = "Allow"
    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeLoadBalancerTargetGroups",
      "autoscaling:DescribeLoadBalancers",
      "autoscaling:DescribeMetricCollectionTypes",
      "autoscaling:DescribePolicies",
      "autoscaling:DescribeScalingActivities",
    ]
    resources = ["*"]
  }
  # Allow management of S3 buckets in this cluster.
  statement {
    effect = "Allow"
    actions = [
      "s3:CreateBucket",
      "s3:DeleteBucket",
      "s3:DeleteBucketPolicy",
      "s3:Get*Configuration",
      "s3:GetBucket*",
      "s3:ListBucket",
      "s3:PutBucket*",
      "s3:Put*Configuration",
    ]
    resources = [
      "arn:aws:s3:::company-gitlab-${var.cluster_name}-*",
    ]
  }
  # Allow management of Elasticsearch domains in this cluster.
  statement {
    effect = "Allow"
    actions = [
      "es:CreateElasticsearchDomain",
      "es:DeleteElasticsearchDomain",
      "es:DescribeElasticsearchDomain",
      "es:DescribeElasticsearchDomainConfig",
      "es:DescribeElasticsearchDomains",
      "es:UpdateElasticsearchDomainConfig",
      "es:UpgradeElasticsearchDomain",
    ]
    resources = [
      "arn:aws:es:*:*:domain/gitlab-${var.cluster_name}-*",
    ]
  }
  # The es:* tagging actions don't seem to allow scoping to
  # individual domains. This feels like a bug on Amazon's side,
  # as the docs at
  # https://docs.aws.amazon.com/IAM/latest/UserGuide/list_amazonelasticsearchservice.html
  # say these actions accept a domain resource. So we grant these
  # apparently buggy actions access to all domains (sadly).
  statement {
    effect = "Allow"
    actions = [
      "es:AddTags",
      "es:ListTags",
      "es:RemoveTags",
    ]
    resources = [
      "arn:aws:es:*:*:domain/*",
    ]
  }
  # Allow management of Route53 zones so we can manage a zone for
  # this cluster. These actions cannot be scoped.
  statement {
    effect = "Allow"
    actions= [
      "route53:CreateHostedZone",
    ]
    resources = ["*"]
  }
  # Allow management of Route53 zones. These actions can be
  # scoped. But only by hosted zone ID, which is random and
  # not known ahead of time. So we effectively can't scope.
  statement {
    effect = "Allow"
    actions = [
      "route53:ChangeResourceRecordSets",
      "route53:ChangeTagsForResource",
      "route53:DeleteHostedZone",
      "route53:GetChange",
      "route53:GetHostedZone",
      "route53:ListResourceRecordSets",
      "route53:ListTagsForResource",
    ]
    resources = [
      "arn:aws:route53:::change/*",
      "arn:aws:route53:::hostedzone/*",
    ]
  }
  # Allow management of ACM certificates. (Non-scopable actions.)
  statement {
    effect = "Allow"
    actions = [
      # AddTagsToCertificate is funky. It says it can be scoped
      # to a certificate resource. But to add tags on certificate
      # creation, the resource has to be *. See
      # https://github.com/terraform-providers/terraform-provider-aws/issues/11903#issuecomment-583337135.
      "acm:AddTagsToCertificate",
      "acm:RequestCertificate",
      "acm:ListTagsForCertificate",
    ]
    resources = ["*"]
  }
  # Allow management of ACM certificates (scopable actions). Most
  # of these can only be scoped to an ID, which is opaque and not
  # known ahead of time. So we can't effectively scope.
  statement {
    effect = "Allow"
    actions = [
      "acm:DeleteCertificate",
      "acm:DescribeCertificate",
      "acm:RemoveTagsFromCertificate",
      "acm:RequestCertificate",
      "acm:UpdateCertificateOptions",
    ]
    resources = [
      "arn:aws:acm:*:${data.aws_caller_identity.current.account_id}:certificate/*",
    ]
  }
}

resource "aws_iam_role_policy" "bootstrap_terraform" {
  name = "terraform"
  role = aws_iam_role.bootstrap.id
  policy = data.aws_iam_policy_document.bootstrap_terraform.json
}

# Upload a zip archive containing the Terraform files for this
# module to the cluster state bucket.
data "archive_file" "terraform_module" {
  type = "zip"
  output_path = "${path.cwd}/terraform_module.zip"
  source_dir = path.module
}

resource "aws_s3_bucket_object" "terraform_module" {
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/terraform_module.zip"
  content_base64 = filebase64(data.archive_file.terraform_module.output_path)
  content_type = "application/zip"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

# Upload a zip archive with Terraform input files for this
# cluster.
data "archive_file" "terraform_cluster" {
  type = "zip"
  output_path = "${path.cwd}/terraform_cluster.zip"
  source {
    filename = "cluster.tf"
    content = file("${path.root}/cluster.tf")
  }
  source {
    filename = "terraform.tf"
    content = file("${path.root}/terraform.tf")
  }
  source {
    filename = "bootstrap_role_initial_policy.json"
    content = file("${path.root}/bootstrap_role_initial_policy.json")
  }
  source {
    filename = "ssh_authorized_keys"
    content = file("${path.root}/ssh_authorized_keys")
  }
}

resource "aws_s3_bucket_object" "terraform_cluster" {
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/terraform_cluster.zip"
  content_base64 = filebase64(data.archive_file.terraform_cluster.output_path)
  content_type = "application/zip"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}
