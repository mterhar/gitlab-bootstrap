variable "aws_region" {
  description = "AWS region to put resources in"
}

variable "bootstrap_bucket_region" {
  description = "AWS region where bootstrap bucket lives"
}

variable "bootstrap_bucket" {
  description = "S3 bucket holding bootstrap files"
}

variable "bootstrap_bucket_prefix" {
  description = "Path prefix in bootstrap S3 bucket where files are rooted. Must contain trailing / if defined."
}

variable "cluster_state_bucket_region" {
  description = "AWS region where cluster state bucket lives"
}

variable "cluster_state_bucket" {
  description = "S3 bucket holding cluster state"
}

variable "cluster_state_bucket_prefix" {
  description = "Path prefix in cluster state S3 bucket where files are rooted. Must contain trailing / if defined"
}

variable "cluster_name" {
  description = "Name of GitLab cluster"
}

variable "bootstrap_role_initial_policy" {
  description = "IAM policy for initial policy attached to bootstrap role."
}

variable "vpc_id" {
  description = "The ID of the VPC in which to run resources"
}

variable "route53_zone_id" {
  description = "AWS resource ID for Route53 zone to place the cluster into"
}

variable "route53_zone_name" {
  description = "Route53 managed DNS zone to place the cluster into"
}

variable "security_groups" {
  description = "Mapping of external security group names to IDs"
  type = map(string)
}

variable "instance_ami" {
  description = "AMI to use for new EC2 instances"
}

variable "availability_zones" {
  description = "List of availability zones to place EC2 resources in"
  type = list(string)
}

variable "subnets_private" {
  description = "List of subnets with private IPs"
  type = list(object({
    id = string
    availability_zone = string
    cidr_block = string
  }))
}

variable "subnets_public" {
  description = "List of subnets with public IPs"
  type = list(object({
    id = string
    availability_zone = string
    cidr_block = string
  }))
}

variable "db_subnet_group_name" {
  description = "RDS subnet group for database resources"
}

variable "elasticache_subnet_group_name" {
  description = "ElastiCache subnet group for Redis clusters"
}

variable "ssh_authorized_keys" {
  description = "SSH authorized keys file to be installed by default on new EC2 instances"
}

variable "gitlab_debian_package" {
  description = "Path to GitLab Omnibus Debian package in bootstrap S3 bucket"
  type = map(string)
  # TODO Install in proper path.
  default = {
    "path": "gitlab-13.0.0.deb",
    "sha256": "d71945e672e75dfd0b635a2a9c357e3211d9d6c4110396f3d9e634021c847bca",
  }
}

variable "consul_instance_count" {
  description = "Number of consul EC2 instances to spawn"
  default = 3
}

variable "consul_instance_type" {
  description = "EC2 instance type for consul instances"
  default = "c5.large"
}

variable "aws_account_id_users" {
  description = "Account ID for company-users"
  default = "42"
}

variable "s3_bucket_suffix" {
  description = "Suffix to use on S3 buckets"
  default = "-dev"
}

variable "s3_logging_bucket" {
  description = "S3 bucket to use for access logging"
  default = "company-s3-access-log-dev"
}

variable "s3_inventory_bucket_arn" {
  description = "ARN of S3 bucket for S3 inventory reports"
  default = "arn:aws:s3:::company-object-inventory-report-internal-only"
}

data "aws_caller_identity" "current" {}

locals {
  cluster_state_arn_prefix = "arn:aws:s3:::${var.cluster_state_bucket}/${var.cluster_state_bucket_prefix}${var.cluster_name}"
  cluster_state_key_prefix = "${var.cluster_state_bucket_prefix}${var.cluster_name}"
  cluster_state_s3_url = "s3://${var.cluster_state_bucket}/${var.cluster_state_bucket_prefix}${var.cluster_name}"
  ec2_instance_arn_prefix = "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:instance"
  iam_policy_arn_prefix = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/gitlab-${var.cluster_name}"
  iam_role_arn_prefix = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/gitlab-${var.cluster_name}"
  iam_instance_profile_arn_prefix = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:instance-profile/gitlab-${var.cluster_name}"
  security_group_arn_prefix = "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:security-group"
  gitlab_debian_package_s3_url = "s3://${var.bootstrap_bucket}/${var.bootstrap_bucket_prefix}${var.gitlab_debian_package["path"]}"
  instance_id_variables_s3_url = "s3://${var.cluster_state_bucket}/${var.cluster_state_bucket_prefix}${var.cluster_name}/ansible-instance-variables/$(curl http://169.254.169.254/latest/meta-data/instance-id).json"
  instance_role_variables_s3_url_prefix = "s3://${var.cluster_state_bucket}/${var.cluster_state_bucket_prefix}${var.cluster_name}/ansible-instance-variables"
  private_subnet_cidr_blocks = {for s in var.subnets_private : s.id => s.cidr_block}
  public_subnet_cidr_blocks = {for s in var.subnets_public : s.id => s.cidr_block}
}
