# This file manages the application services component of GitLab.
# This is a horizontally scalable layer of EC2 instances to handle
# web requests, SSH connections, etc.

variable "app_server_instance_type" {
  description = "EC2 instance type for application services instances"
  default = "c5.4xlarge"
}

variable "app_server_lb_deregistration_delay" {
  description = "Delay between when an app_server instance starts draining in load balancer to when it is deregistered"
  type = number
  # This should probably be increased in production. Short values
  # are useful for development.
  default = 30
}

variable "app_server_asg_min_size" {
  description = "Minimum number of EC2 instances in app server auto scaling group"
  type = number
  default = 1
}

variable "app_server_asg_max_size" {
  description = "Maximum number of EC2 instances in app server auto scaling group"
  type = number
  default = 3
}

variable "app_server_availability_zones" {
  description = "How many availability zones to place instances into"
  type = number
  # Just 1 by default to keep costs in check.
  default = 1
}

resource "aws_iam_role" "app_server" {
  name = "gitlab-${var.cluster_name}-app_server"
  description = "Role for GitLab application services EC2 instances for ${var.cluster_name}"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
}

resource "aws_iam_instance_profile" "app_server" {
  name = aws_iam_role.app_server.name
  role = aws_iam_role.app_server.name
}

data "aws_iam_policy_document" "app_server" {
  # Allow EC2 instances to download gitlab.rb config file.
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "${local.cluster_state_arn_prefix}/app_server_gitlab.rb",
    ]
  }
}

resource "aws_iam_role_policy" "app_server" {
  role = aws_iam_role.app_server.name
  policy = data.aws_iam_policy_document.app_server.json
}

resource "aws_iam_role_policy_attachment" "app_server_ansible" {
  role = aws_iam_role.app_server.name
  policy_arn = aws_iam_policy.ansible.arn
}

resource "aws_iam_role_policy_attachment" "app_server_s3_storage" {
  role = aws_iam_role.app_server.name
  policy_arn = aws_iam_policy.s3_bucket_read_write.arn
}

resource "aws_iam_role_policy_attachment" "app_server_elasticsearch" {
  role = aws_iam_role.app_server.name
  policy_arn = aws_iam_policy.elasticsearch_domain_access.arn
}

resource "aws_security_group" "app_server" {
  name = "gitlab-${var.cluster_name}-app_server"
  description = "Applied to application services EC2 instances for ${var.cluster_name}"
  vpc_id = var.vpc_id
  # Allow incoming connections from load balancer, which is placed
  # in a subnet. We don't know where in the subnet it is placed. So
  # we have to allow the entire subnet cidr block.
  ingress {
    description = "Allow SSH from load balancer subnet"
    protocol = "tcp"
    from_port = 22
    to_port = 22
    cidr_blocks = [for id in aws_lb.app_server.subnets : local.private_subnet_cidr_blocks[id]]
  }
  ingress {
    description = "Allow HTTP from load balancer subnet"
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = [for id in aws_lb.app_server.subnets : local.private_subnet_cidr_blocks[id]]
  }
  ingress {
    description = "Allow HTTP for Registry from load balancer subnet"
    protocol = "tcp"
    from_port = 5050
    to_port = 5050
    cidr_blocks = [for id in aws_lb.app_server.subnets : local.private_subnet_cidr_blocks[id]]
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
}

resource "aws_launch_template" "app_server" {
  name = "gitlab-${var.cluster_name}-app_server"
  description = "Used to launch GitLab application services instances for ${var.cluster_name}"
  iam_instance_profile{
    arn = aws_iam_instance_profile.app_server.arn
  }
  image_id = var.instance_ami
  # Instances are stateless. So shutdown results in termination.
  instance_initiated_shutdown_behavior = "terminate"
  instance_type = var.app_server_instance_type
  vpc_security_group_ids = [
    aws_security_group.app_server.id,
  ]
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "gitlab"
      # TODO is this accurate? Technically it is managed by an ASG
      # which is managed by Terraform...
      ManagedBy = "terraform"
      GitLabCluster = var.cluster_name
      GitLabComponent = "app_server"
    }
  }
  tag_specifications {
    resource_type = "volume"
    tags = {
      Name = "gitlab"
      ManagedBy = "terraform"
      GitLabCluster = var.cluster_name
      GitLabComponent = "app_server"
    }
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
  user_data = base64encode(templatefile("${path.module}/gitlab_user_data.sh", {
    ansible_variables_s3_url = "${local.instance_role_variables_s3_url_prefix}/app_server.json"
    gitlab_cluster_state_s3_url = "s3://${var.cluster_state_bucket}/${local.cluster_state_key_prefix}"
    gitlab_role = "app_server"
    ssh_authorized_keys = var.ssh_authorized_keys
    ignore_cluster_ready_semaphore = "false"
  }))
}

resource "aws_lb" "app_server" {
  name = "gitlab-${var.cluster_name}-app-server"
  # TODO we need to allow inbound access from these subnets to the
  # security group for the app_server EC2 instances. It would be
  # ideal to use a smaller scoped subnet - perhaps a subnet dedicated
  # just to load balancers, preferably just our load balancer.
  subnets = [for i in range(var.app_server_availability_zones): var.subnets_private[i].id]
  # ALBs only support HTTP/HTTPS.
  # NLBs only support TCP/UDP/TLS.
  # We need to support both SSH and HTTP and are therefore limited
  # to an NLB.
  load_balancer_type = "network"
  # TODO this will need to be external someday.
  internal = true
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
}

resource "aws_lb_target_group" "app_server_ssh" {
  name = "gitlab-${var.cluster_name}-ssh"
  port = 22
  protocol = "TCP"
  vpc_id = var.vpc_id
  deregistration_delay = var.app_server_lb_deregistration_delay
  health_check {
    port = 80
    protocol = "HTTP"
    path = "/users/sign_in"
    interval = 10
    healthy_threshold = 3
    unhealthy_threshold = 3
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
}

resource "aws_lb_listener" "app_server_22" {
  load_balancer_arn = aws_lb.app_server.arn
  port = 22
  protocol = "TCP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.app_server_ssh.arn
  }
}

resource "aws_lb_target_group" "app_server_https" {
  name = "gitlab-${var.cluster_name}-https"
  # Traffic goes to port 80 since TLS is terminated here.
  port = 80
  protocol = "TCP"
  vpc_id = var.vpc_id
  deregistration_delay = var.app_server_lb_deregistration_delay
  health_check {
    port = 80
    protocol = "HTTP"
    path = "/users/sign_in"
    interval = 10
    healthy_threshold = 3
    unhealthy_threshold = 3
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
}

resource "aws_lb_listener" "app_server_https" {
  load_balancer_arn = aws_lb.app_server.arn
  port = 443
  protocol = "TLS"
  certificate_arn = aws_acm_certificate.main.arn
  ssl_policy = "ELBSecurityPolicy-FS-1-2-2019-08"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.app_server_https.arn
  }
}

resource "aws_lb_target_group" "app_server_registry" {
  name = "gitlab-${var.cluster_name}-registry"
  port = 5050
  protocol = "TCP"
  vpc_id = var.vpc_id
  deregistration_delay = var.app_server_lb_deregistration_delay
  health_check {
    port = 80
    protocol = "HTTP"
    path = "/users/sign_in"
    interval = 10
    healthy_threshold = 3
    unhealthy_threshold = 3
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
}

resource "aws_lb_listener" "app_server_registry" {
  load_balancer_arn = aws_lb.app_server.arn
  port = 5050
  protocol = "TLS"
  certificate_arn = aws_acm_certificate.main.arn
  ssl_policy = "ELBSecurityPolicy-FS-1-2-2019-08"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.app_server_registry.arn
  }
}

# TODO define a placement group to ensure better instance isolation.
resource "aws_autoscaling_group" "app_server" {
  name = "gitlab-${var.cluster_name}-app_server"
  min_size = var.app_server_asg_min_size
  max_size = var.app_server_asg_max_size
  launch_template {
    id = aws_launch_template.app_server.id
    version = "$Latest"
  }
  health_check_type = "ELB"
  vpc_zone_identifier = aws_lb.app_server.subnets[*]
  termination_policies = ["OldestInstance", "OldestLaunchConfiguration"]
  target_group_arns = [
    aws_lb_target_group.app_server_ssh.arn,
    aws_lb_target_group.app_server_https.arn,
    aws_lb_target_group.app_server_registry.arn,
  ]
  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceCapacity",
    "GroupPendingCapacity",
    "GroupMinSize",
    "GroupMaxSize",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupStandbyCapacity",
    "GroupTerminatingCapacity",
    "GroupTerminatingInstances",
    "GroupTotalCapacity",
    "GroupTotalInstances",
  ]
  tags = [
    {key: "Name", value: "gitlab", propagate_at_launch: true},
    {key: "ManagedBy", value: "terraform", propagate_at_launch: true},
    {key: "GitLabCluster", value: var.cluster_name, propagate_at_launch: true},
    {key: "GitLabComponent", value: "app_server", propagate_at_launch: true},
  ]
}

resource "aws_autoscaling_policy" "app_server_cpu" {
  name = "gitlab-${var.cluster_name}-app_server_cpu"
  autoscaling_group_name = aws_autoscaling_group.app_server.name
  adjustment_type = "ChangeInCapacity"
  policy_type = "TargetTrackingScaling"
  estimated_instance_warmup = 150
  metric_aggregation_type = "Average"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 60.0
  }
}

locals {
  # The Ansible variables for app server are shared with sidekiq.
  app_server_common_ansible_variables = {
    gitlab_debian_package_s3_url = local.gitlab_debian_package_s3_url
    gitlab_debian_package_sha256 = var.gitlab_debian_package.sha256

    cluster_state_s3_url = local.cluster_state_s3_url
    skip_gitlab_secrets_download = false

    postgres_primary_address: local.postgres_primary_address
    postgres_primary_port: local.postgres_primary_port
    postgres_primary_username: local.postgres_primary_username
    postgres_primary_password: local.postgres_primary_password
    postgres_primary_db_name: local.postgres_primary_db_name

    redis_host = aws_elasticache_cluster.primary.cache_nodes[0].address
    redis_port = aws_elasticache_cluster.primary.cache_nodes[0].port

    cluster_hostname = aws_acm_certificate.main.domain_name
    cluster_url = "https://${aws_acm_certificate.main.domain_name}"

    consul_hostnames = local.consul_hostnames
    monitor_ip = aws_instance.monitor.private_ip

    praefect_lb_address = aws_lb.praefect.dns_name
    praefect_internal_token: random_password.praefect_internal_token.result
    praefect_external_token = random_password.praefect_external_token.result

    artifacts_s3_bucket = module.s3_bucket_artifacts.bucket
    artifacts_s3_bucket_region = module.s3_bucket_artifacts.region
    external_diffs_s3_bucket = module.s3_bucket_external_diffs.bucket
    external_diffs_s3_bucket_region = module.s3_bucket_external_diffs.region
    lfs_s3_bucket = module.s3_bucket_lfs.bucket
    lfs_s3_bucket_region = module.s3_bucket_lfs.region
    registry_s3_bucket = module.s3_bucket_registry.bucket
    registry_s3_bucket_region = module.s3_bucket_registry.region
    uploads_s3_bucket = module.s3_bucket_uploads.bucket
    uploads_s3_bucket_region = module.s3_bucket_uploads.region
  }
}

resource "aws_s3_bucket_object" "app_server_ansible_variables" {
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/ansible-instance-variables/app_server.json"
  content = jsonencode(
    merge(
      local.app_server_common_ansible_variables,
      {
        role = "app_server"
      }
    )
  )
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "app_server"
  }
}
