#!/usr/bin/env bash
# Shell script executed on launch of application services instances.
#
# This file is evaluated as a Terraform template. So $ + {
# needs to be escaped as $ + $ + {.

set -exo pipefail

cat > /root/.ssh/authorized_keys << EOF
${ssh_authorized_keys}
EOF

cat > /etc/apt/apt.conf.d/99gitlab << EOF
APT::Get::Assume-Yes "true";
APT::Install-Recommends "false";
Acquire::Retries "5";
EOF

export DEBIAN_FRONTEND=noninteractive

apt update
apt upgrade
apt install \
  ansible \
  awscli

# Write out a simple shell script that syncs Ansible state.
cat >/root/sync-ansible <<EOF
#!/usr/bin/env bash
set -exo pipefail

echo "synchronizing Ansible files from S3 bucket..."

# Copy latest Ansible files from S3.
#
# The files are stored as a tar archive so they can be fetched and
# updated atomically. We nuke out the local copy to ensure no deleted
# files linger.
aws s3 cp ${gitlab_cluster_state_s3_url}/ansible/latest.tar /root/ansible-latest.tar
rm -rf /root/ansible
tar -C /root -x -f /root/ansible-latest.tar

# The admin instance needs to perform some one-off changes
# before the application nodes do anything meaningful. So we
# wait on that semaphore file to come into existence before doing
# anything. (Unless we override this behavior.)
if [ "${ignore_cluster_ready_semaphore}" != "true" ]; then
  until [ -f /root/cluster_ready_semaphore ]; do
     aws s3 cp \
       ${gitlab_cluster_state_s3_url}/ansible/cluster_ready_semaphore \
       /root/cluster_ready_semaphore || sleep 5
  done
fi

# Each EC2 instance has an S3 object named after its instance ID
# containing per-instance variables.
#
# Fetch the latest version of that file, waiting for it to come into
# existence as necessary.

echo "fetching latest Ansible instance variables..."
rm -f /etc/ansible_instance_vars.json

until [ -f /etc/ansible_instance_vars.json ]; do
  aws s3 cp \
    ${ansible_variables_s3_url} \
    /etc/ansible_instance_vars.json || sleep 5
done

# Fetch one-off variables managed by other means. (This file is optional.)
rm -f /etc/ansible_extra_instance_variables.yml
aws s3 cp \
  ${gitlab_cluster_state_s3_url}/ansible/extra_instance_variables.yml \
  /etc/ansible_extra_instance_variables.yml || true

EOF

chmod +x /root/sync-ansible

# Write out a "converge" script that does an Ansible files sync
# and runs Ansible.
cat >/root/converge <<EOF
#!/usr/bin/env bash

set -exo pipefail

/root/sync-ansible

EXTRA_ARGS=
if [ -f /etc/ansible_extra_instance_variables.yml ]; then
  EXTRA_ARGS="--extra-vars @/etc/ansible_extra_instance_variables.yml"
fi

ansible-playbook \
  --extra-vars @/etc/ansible_instance_vars.json \
  \$EXTRA_ARGS \
  /root/ansible/launch-${gitlab_role}.yml
EOF

chmod +x /root/converge

# Do initial converge.
/root/converge

touch /etc/user_data_finished
