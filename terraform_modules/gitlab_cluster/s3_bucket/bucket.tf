variable "bucket" {
  description = "name of S3 bucket"
}

variable "region" {
  description = "AWS region to put bucket in"
}

variable "cluster_name" {
  description = "Name of GitLab cluster"
}

variable "logging_bucket" {
  description = "Name of logging bucket"
}

variable "inventory_bucket_arn" {
  description = "ARN of S3 bucket to send inventory reports to"
}

variable "users_account_id" {
  description = "AWS account id for company-users"
}

resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket
  region = var.region
  acl = "private"
  force_destroy = false
  versioning {
    enabled = true
  }
  logging {
    target_bucket = var.logging_bucket
    target_prefix = "${var.bucket}-audit/"
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  lifecycle_rule {
    prefix = ""
    enabled = true
    abort_incomplete_multipart_upload_days = 2
    expiration {
      expired_object_delete_marker = true
    }
    noncurrent_version_expiration {
      days = 14
    }
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    exception = "false"
    exception_expiry = "n/a"
  }
}

data "aws_iam_policy_document" "bucket" {
  statement {
    sid = "DenyPutObjectACL"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:PutObjectACL",
    ]
    resources = [
      "${aws_s3_bucket.bucket.arn}/*",
    ]
  }
  statement {
    sid = "EnforceTLSOnlyAccess"
    effect = "Deny"
    principals {
      type = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:*",
    ]
    resources = [
      aws_s3_bucket.bucket.arn,
      "${aws_s3_bucket.bucket.arn}/*",
    ]
    condition {
      test = "Bool"
      variable = "aws:SecureTransport"
      values = ["false"]
    }
  }
  statement {
    sid = "DenyCrossAccountAccessNoMFA"
    effect = "Deny"
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${var.users_account_id}:root",
      ]
    }
    actions = [
      "s3:*",
    ]
    resources = [
      aws_s3_bucket.bucket.arn,
    ]
    condition {
      test = "BoolIfExists"
      variable = "aws:MultiFactorAuthPresent"
      values = ["false"]
    }
  }
  # TODO allow disaster recovery access.
}

resource "aws_s3_bucket_policy" "bucket" {
  bucket = aws_s3_bucket.bucket.bucket
  policy = data.aws_iam_policy_document.bucket.json
}

resource "aws_s3_bucket_metric" "bucket" {
  bucket = aws_s3_bucket.bucket.id
  name = "EntireBucket"
}

resource "aws_s3_bucket_inventory" "bucket" {
  bucket = aws_s3_bucket.bucket.id
  name = "inventory"
  included_object_versions = "All"
  schedule {
    frequency = "Weekly"
  }
  destination {
    bucket {
      format = "CSV"
      bucket_arn = var.inventory_bucket_arn
    }
  }
  optional_fields = [
    "Size",
    "LastModifiedDate",
    "StorageClass",
    "ETag",
    "IsMultipartUploaded",
    "ReplicationStatus",
    "EncryptionStatus",
  ]
}

output "bucket" {
  description = "Name of S3 bucket"
  value = aws_s3_bucket.bucket.bucket
}

output "arn" {
  description = "ARN of S3 bucket"
  value = aws_s3_bucket.bucket.arn
}

output "region" {
  description = "Name of AWS region of S3 bucket"
  value = aws_s3_bucket.bucket.region
}
