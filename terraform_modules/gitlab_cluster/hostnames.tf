# Each cluster gets its own Route53 DNS zone.
#
# This zone is a child of the parent zone passed in via input
# variables.

resource "aws_route53_zone" "cluster" {
  name = "${var.cluster_name}.${var.route53_zone_name}"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

# Create NS record in parent zone.
resource "aws_route53_record" "cluster_ns" {
  zone_id = var.route53_zone_id
  name = aws_route53_zone.cluster.name
  type = "NS"
  ttl = "60"
  records = aws_route53_zone.cluster.name_servers
}

# Create A record pointing to ELB.
resource "aws_route53_record" "zone_a" {
  zone_id = aws_route53_zone.cluster.zone_id
  name = aws_route53_zone.cluster.name
  type = "A"
  alias {
    name = aws_lb.app_server.dns_name
    zone_id = aws_lb.app_server.zone_id
    evaluate_target_health = true
  }
}

# Create an x509 certificate for the zone.
resource "aws_acm_certificate" "main" {
  domain_name = aws_route53_record.zone_a.name
  validation_method = "DNS"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

resource "aws_route53_record" "zone_validation" {
  name = aws_acm_certificate.main.domain_validation_options.0.resource_record_name
  type = aws_acm_certificate.main.domain_validation_options.0.resource_record_type
  zone_id = aws_route53_zone.cluster.zone_id
  records = [
    aws_acm_certificate.main.domain_validation_options.0.resource_record_value,
  ]
  ttl = 60
}

resource "aws_acm_certificate_validation" "zone" {
  certificate_arn = aws_acm_certificate.main.arn
  validation_record_fqdns = [
    aws_route53_record.zone_validation.fqdn,
  ]
}
