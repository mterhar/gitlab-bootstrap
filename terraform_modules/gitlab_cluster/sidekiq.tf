# This file manages the Sidekiq component of GitLab. This is a
# horizontally scalable layer of EC2 instance that process background
# jobs.

variable "sidekiq_instance_type" {
  description = "EC2 instance type for Sidekiq instances"
  default = "c5.xlarge"
}

variable "sidekiq_asg_min_size" {
  description = "Minimum number of EC2 instances in sidekiq auto scaling group"
  type = number
  default = 1
}

variable "sidekiq_asg_max_size" {
  description = "Maximum number of EC2 instances in sidekiq auto scaling group"
  type = number
  default = 3
}

resource "aws_iam_role" "sidekiq" {
  name = "gitlab-${var.cluster_name}-sidekiq"
  description = "Role for GitLab sidekiq EC2 instances for ${var.cluster_name}"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "sidekiq"
  }
}

resource "aws_iam_instance_profile" "sidekiq" {
  name = aws_iam_role.sidekiq.name
  role = aws_iam_role.sidekiq.name
}

resource "aws_iam_role_policy_attachment" "sidekiq_ansible" {
  role = aws_iam_role.sidekiq.name
  policy_arn = aws_iam_policy.ansible.arn
}

resource "aws_iam_role_policy_attachment" "sidekiq_s3_storage" {
  role = aws_iam_role.sidekiq.name
  policy_arn = aws_iam_policy.s3_bucket_read_write.arn
}

resource "aws_iam_role_policy_attachment" "sidekiq_elasticsearch" {
  role = aws_iam_role.sidekiq.name
  policy_arn = aws_iam_policy.elasticsearch_domain_access.arn
}

resource "aws_security_group" "sidekiq" {
  name = "gitlab-${var.cluster_name}-sidekiq"
  description = "Applied to Sidekiq EC2 instances for ${var.cluster_name}"
  vpc_id = var.vpc_id
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "sidekiq"
  }
}

resource "aws_launch_template" "sidekiq" {
  name = "gitlab-${var.cluster_name}-sidekiq"
  description = "Used to launch GitLab sidekiq instances for ${var.cluster_name}"
  iam_instance_profile {
    arn = aws_iam_instance_profile.sidekiq.arn
  }
  image_id = var.instance_ami
  # Instances are stateless. So shutdown results in termination.
  instance_initiated_shutdown_behavior = "terminate"
  instance_type = var.sidekiq_instance_type
  vpc_security_group_ids = [
    aws_security_group.sidekiq.id,
  ]
  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "gitlab"
      # TODO is this accurate? Technically it is managed by an ASG
      # which is managed by Terraform...
      ManagedBy = "terraform"
      GitLabCluster = var.cluster_name
      GitLabComponent = "sidekiq"
    }
  }
  tag_specifications {
    resource_type = "volume"
    tags = {
      Name = "gitlab"
      ManagedBy = "terraform"
      GitLabCluster = var.cluster_name
      GitLabComponent = "sidekiq"
    }
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
  user_data = base64encode(templatefile("${path.module}/gitlab_user_data.sh", {
    ansible_variables_s3_url = "${local.instance_role_variables_s3_url_prefix}/sidekiq.json"
    gitlab_cluster_state_s3_url = "s3://${var.cluster_state_bucket}/${local.cluster_state_key_prefix}",
    ssh_authorized_keys = var.ssh_authorized_keys,
    gitlab_role = "sidekiq",
    ignore_cluster_ready_semaphore = "false"
  }))
}

# TODO define a placement group to ensure better instance isolation.
resource "aws_autoscaling_group" "sidekiq" {
  name = "gitlab-${var.cluster_name}-sidekiq"
  min_size = var.sidekiq_asg_min_size
  max_size = var.sidekiq_asg_max_size
  launch_template {
    id = aws_launch_template.sidekiq.id
    version = "$Latest"
  }
  health_check_type = "EC2"
  vpc_zone_identifier = var.subnets_private[*].id
  termination_policies = ["OldestInstance", "OldestLaunchConfiguration"]
  enabled_metrics = [
    "GroupDesiredCapacity",
    "GroupInServiceCapacity",
    "GroupPendingCapacity",
    "GroupMinSize",
    "GroupMaxSize",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupStandbyCapacity",
    "GroupTerminatingCapacity",
    "GroupTerminatingInstances",
    "GroupTotalCapacity",
    "GroupTotalInstances",
  ]
  tags = [
    {key: "Name", value: "gitlab", propagate_at_launch: true},
    {key: "ManagedBy", value: "terraform", propagate_at_launch: true},
    {key: "GitLabCluster", value: var.cluster_name, propagate_at_launch: true},
    {key: "GitLabComponent", value: "sidekiq", propagate_at_launch: true},
  ]
}

resource "aws_autoscaling_policy" "sidekiq_cpu" {
  name = "gitlab-${var.cluster_name}-sidekiq_cpu"
  autoscaling_group_name = aws_autoscaling_group.sidekiq.name
  adjustment_type = "ChangeInCapacity"
  policy_type = "TargetTrackingScaling"
  estimated_instance_warmup = 150
  metric_aggregation_type = "Average"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 25.0
  }
}

resource "aws_s3_bucket_object" "sidekiq_ansible_variables" {
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/ansible-instance-variables/sidekiq.json"
  content = jsonencode(
    merge(
      local.app_server_common_ansible_variables,
      {
        role = "sidekiq"
      }
    )
  )
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "sidekiq"
  }
}
