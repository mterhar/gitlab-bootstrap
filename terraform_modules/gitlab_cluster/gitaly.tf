# This file manages the Gitaly components of a GitLab cluster.
#
# TODO our single node deployment is not yet HA. Adopt praefect
# once it is stable enough.

variable "gitaly_instance_type" {
  description = "EC2 instance type for Gitaly nodes"
  default = "c5d.9xlarge"
}

variable "gitaly_ebs_volume_size" {
  description = "Size in gigabytes of Gitaly EBS volume"
  type = number
  # Small by default to keep costs in check.
  default = 16
}

resource "aws_iam_role" "gitaly" {
  name = "gitlab-${var.cluster_name}-gitaly"
  description = "Role for Gitaly servers for ${var.cluster_name}"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "gitaly"
  }
}

resource "aws_iam_instance_profile" "gitaly" {
  name = aws_iam_role.gitaly.name
  role = aws_iam_role.gitaly.name
}

resource "aws_iam_role_policy_attachment" "gitaly_ansible" {
  role = aws_iam_role.gitaly.name
  policy_arn = aws_iam_policy.ansible.arn
}

resource "aws_security_group" "gitaly" {
  name = "gitlab-${var.cluster_name}-gitaly"
  description = "Applied to Gitaly EC2 instances for ${var.cluster_name}"
  vpc_id = var.vpc_id
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "gitaly"
  }
}

resource "aws_instance" "gitaly_main" {
  count = 2
  instance_type = var.gitaly_instance_type
  ami = var.instance_ami
  iam_instance_profile = aws_iam_instance_profile.gitaly.name
  associate_public_ip_address = false
  availability_zone = var.subnets_private[0].availability_zone
  subnet_id = var.subnets_private[0].id
  vpc_security_group_ids = [
    aws_security_group.gitaly.id,
  ]
  user_data = templatefile("${path.module}/gitlab_user_data.sh", {
    ansible_variables_s3_url = local.instance_id_variables_s3_url
    gitlab_cluster_state_s3_url = "s3://${var.cluster_state_bucket}/${local.cluster_state_key_prefix}",
    gitlab_role = "gitaly",
    ssh_authorized_keys = var.ssh_authorized_keys,
    ignore_cluster_ready_semaphore = "false"
  })
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "gitaly"
  }
  volume_tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "gitaly"
  }
}

resource "aws_s3_bucket_object" "gitaly_main_ansible_variables" {
  count = length(aws_instance.gitaly_main)
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/ansible-instance-variables/${aws_instance.gitaly_main[count.index].id}.json"
  content = jsonencode({
    gitlab_debian_package_s3_url: local.gitlab_debian_package_s3_url
    gitlab_debian_package_sha256: var.gitlab_debian_package.sha256

    cluster_state_s3_url = local.cluster_state_s3_url
    skip_gitlab_secrets_download = false

    cluster_hostname = aws_acm_certificate.main.domain_name
    cluster_url = "https://${aws_acm_certificate.main.domain_name}"

    consul_hostnames = local.consul_hostnames
    praefect_internal_token = random_password.praefect_internal_token.result

    gitaly_instance_count = length(aws_instance.gitaly_main)
  })
  content_type = "application/json"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "gitaly"
  }
}

# Define a hostnames for EC2 instances, for convenience.
resource "aws_route53_record" "gitaly_main" {
  count = length(aws_instance.gitaly_main)
  zone_id = aws_route53_zone.cluster.id
  name = "gitaly-main-${count.index}.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = "60"
  records = [aws_instance.gitaly_main[count.index].private_ip]
}

resource "aws_route53_record" "gitaly_main_all" {
  zone_id = aws_route53_zone.cluster.id
  name = "gitaly-main-all.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = "60"
  records = aws_instance.gitaly_main[*].private_ip
}
