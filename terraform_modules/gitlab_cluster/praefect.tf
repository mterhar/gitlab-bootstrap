# Praefect is a Gitaly proxy/router.

variable "praefect_instance_count" {
  description = "Number of Praefect EC2 instances"
  type = number
  default = 1
}

variable "praefect_instance_type" {
  description = "EC2 instance type for Praefect"
  default = "c5.2xlarge"
}

variable "praefect_lb_deregistration_delay" {
  description = "Load balancer deregistration delay for Praefect"
  default = 30
}

resource "random_password" "praefect_external_token" {
  length = 24
}

resource "random_password" "praefect_internal_token" {
  length = 24
}

resource "aws_security_group" "praefect_database" {
  name = "gitlab-${var.cluster_name}-praefect_database"
  description = "Security group for Praefect database for ${var.cluster_name}"
  vpc_id = var.vpc_id
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
}

module "praefect_database" {
  source = "./postgres_database"
  cluster_name = var.cluster_name
  identifier = "praefect"
  db_name = "praefect"
  availability_zone = var.subnets_private[0].availability_zone
  subnet_group_name = var.db_subnet_group_name
  security_group_ids = [
    aws_security_group.praefect_database.id,
  ]
}

locals {
  praefect_db_address = module.praefect_database.address
  praefect_db_port = module.praefect_database.port
  praefect_db_username = module.praefect_database.username
  praefect_db_password = module.praefect_database.password
  praefect_db_name = module.praefect_database.db_name
}

resource "aws_iam_role" "praefect" {
  name = "gitlab-${var.cluster_name}-praefect"
  description = "Role for Praefect EC2 instances for ${var.cluster_name}"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
}

resource "aws_iam_instance_profile" "praefect" {
  name = aws_iam_role.praefect.name
  role = aws_iam_role.praefect.name
}

resource "aws_iam_role_policy_attachment" "praefect_ansible" {
  role = aws_iam_role.praefect.name
  policy_arn = aws_iam_policy.ansible.arn
}

resource "aws_security_group" "praefect" {
  name = "gitlab-${var.cluster_name}-praefect"
  description = "Security group for Praefect EC2 instances for ${var.cluster_name}"
  vpc_id = var.vpc_id
  # Allow incoming connections from load balancer, which is placed
  # in a subnet. We don't know where in the subnet it is placed. So
  # we have to allow the entire subnet cidr block.
  ingress {
    description = "Allow Praefect connections from load balancer"
    protocol = "tcp"
    from_port = 2305
    to_port = 2305
    # Ideall this would reference subnets on LB. But that would create
    # a dependency cycle.
    cidr_blocks = [for id in var.subnets_private[*].id : local.private_subnet_cidr_blocks[id]]
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
}

resource "aws_instance" "praefect" {
  count = var.praefect_instance_count
  instance_type = var.praefect_instance_type
  ami = var.instance_ami
  iam_instance_profile = aws_iam_instance_profile.praefect.name
  associate_public_ip_address = false
  # TODO needs modulus
  availability_zone = var.subnets_private[count.index].availability_zone
  subnet_id = var.subnets_private[count.index].id
  vpc_security_group_ids = [
    aws_security_group.praefect.id,
  ]
  user_data = templatefile("${path.module}/gitlab_user_data.sh", {
    ansible_variables_s3_url = local.instance_id_variables_s3_url
    gitlab_cluster_state_s3_url = local.cluster_state_s3_url
    gitlab_role = "praefect"
    ssh_authorized_keys = var.ssh_authorized_keys
    ignore_cluster_ready_semaphore = "false"
  })
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
  volume_tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
}

# Each Praefect instance receives a DNS record for that instance.
resource "aws_route53_record" "praefect" {
  count = length(aws_instance.praefect)
  zone_id = aws_route53_zone.cluster.id
  name = "praefect-${count.index}.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = "60"
  records = [aws_instance.praefect[count.index].private_ip]
}

# We also expose a record for all Praefect instances, to allow
# for easy querying.
resource "aws_route53_record" "praefect_all" {
  zone_id = aws_route53_zone.cluster.id
  name = "praefect-all.${aws_route53_zone.cluster.name}"
  type = "A"
  ttl = 60
  records = aws_instance.praefect[*].private_ip
}

locals {
  praefect_common_variables = {
    praefect_lb_address = aws_lb.praefect.dns_name
    praefect_external_token = random_password.praefect_external_token.result
    praefect_internal_token = random_password.praefect_internal_token.result
    praefect_db_address = local.praefect_db_address
    praefect_db_port = local.praefect_db_port
    praefect_db_username = local.praefect_db_username
    praefect_db_password = local.praefect_db_password
    praefect_db_name = local.praefect_db_name
    gitaly_main_ips = aws_instance.gitaly_main[*].private_ip
  }
}

resource "aws_s3_bucket_object" "praefect_ansible_variables" {
  count = length(aws_instance.praefect)
  bucket = var.cluster_state_bucket
  key = "${local.cluster_state_key_prefix}/ansible-instance-variables/${aws_instance.praefect[count.index].id}.json"
  content = jsonencode(merge(
    local.praefect_common_variables, {
    gitlab_debian_package_s3_url = local.gitlab_debian_package_s3_url
    gitlab_debian_package_sha256 = var.gitlab_debian_package.sha256
    cluster_state_s3_url = local.cluster_state_s3_url
    skip_gitlab_secrets_download = false
    cluster_url = "https://${aws_acm_certificate.main.domain_name}"
    consul_hostnames = local.consul_hostnames
  }))
  content_type = "application/json"
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
}

# Use an NLB to send traffic to Praefect instances.
resource "aws_lb" "praefect" {
  name = "gitlab-${var.cluster_name}-praefect"
  subnets = aws_instance.praefect[*].subnet_id
  load_balancer_type = "network"
  internal = true
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
}

resource "aws_lb_target_group" "praefect" {
  name = "gitlab-${var.cluster_name}-praefect"
  port = 2305
  protocol = "TCP"
  vpc_id = var.vpc_id
  deregistration_delay = var.praefect_lb_deregistration_delay
  target_type = "instance"
  health_check {
    protocol = "TCP"
    interval = 10
    healthy_threshold = 3
    unhealthy_threshold = 3
  }
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
    GitLabComponent = "praefect"
  }
}

resource "aws_lb_listener" "praefect" {
  load_balancer_arn = aws_lb.praefect.arn
  port = 2305
  protocol = "TCP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.praefect.arn
  }
}

# Attach each EC2 instance to the load balancer.
resource "aws_lb_target_group_attachment" "praefect" {
  count = length(aws_instance.praefect)
  target_group_arn = aws_lb_target_group.praefect.arn
  target_id = aws_instance.praefect[count.index].id
  port = 2305
}
