# This file defines resources for an Elasticsearch cluster.

variable "elasticsearch_instance_type" {
  description = "Instance type to use for Elaticsearch nodes"
  # Smallest instance type to keep costs in check by default.
  default = "m5.2xlarge.elasticsearch"
}

variable "elasticsearch_instance_count" {
  description = "Number of nodes in Elasticsearch cluster"
  type = number
  # Just 1 by default to keep costs in check.
  default = 1
}

variable "elasticsearch_volume_type" {
  description = "EBS volume type to use with Elasticsearch nodes"
  default = "gp2"
}

variable "elasticsearch_volume_size" {
  description = "EBS volume size for Elasticsearch nodes"
  # Small by default to keep costs in check.
  default = 64
}

resource "aws_security_group" "elaticsearch" {
  name = "gitlab-${var.cluster_name}-elasticsearch"
  description = "Applied to Elasticsearch instances for ${var.cluster_name}"
  vpc_id = var.vpc_id
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

data "aws_iam_policy_document" "elasticsearch" {
  # We have to install a default policy. This policy is
  # effectively a no-op since it grants a minimal access to
  # self from self.
  statement {
    effect = "Allow"
    actions = ["es:ESHttpHead"]
    principals {
      type = "*"
      identifiers = ["arn:aws:es:*:*:domain/gitlab-${var.cluster_name}-main"]
    }
    resources = [
      "arn:aws:es:*:*:domain/gitlab-${var.cluster_name}-main"
    ]
  }
}

# This resource technically depends on a service linked role
# with service name "es.amazonaws.com". This role may not be
# present in fresh AWS accounts. But if there's already an
# ES instance in the account, it probably exists. So we don't
# add it here.
#
# TODO support multiple AZs
resource "aws_elasticsearch_domain" "main" {
  domain_name = "gitlab-${var.cluster_name}-main"
  elasticsearch_version = "7.4"
  cluster_config {
    instance_type = var.elasticsearch_instance_type
    instance_count = var.elasticsearch_instance_count
    dedicated_master_enabled = false
    zone_awareness_enabled = false
  }
  vpc_options {
    security_group_ids = [
      aws_security_group.elaticsearch.id,
    ]
    subnet_ids = [var.subnets_private[0].id]
  }
  ebs_options {
    ebs_enabled = true
    volume_type = var.elasticsearch_volume_type
    volume_size = var.elasticsearch_volume_size
  }
  encrypt_at_rest {
    enabled = true
  }
  node_to_node_encryption {
    enabled = true
  }
  domain_endpoint_options {
    enforce_https = true
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }
  snapshot_options {
    automated_snapshot_start_hour = 7
  }
  access_policies = data.aws_iam_policy_document.elasticsearch.json
  tags = {
    Name = "gitlab"
    ManagedBy = "terraform"
    GitLabCluster = var.cluster_name
  }
}

data "aws_iam_policy_document" "elasticsearch_domain_access" {
  statement {
    effect = "Allow"
    actions = [
      "es:ESHttpDelete",
      "es:ESHttpGet",
      "es:ESHttpHead",
      "es:ESHttpPatch",
      "es:ESHttpPost",
      "es:ESHttpPut",
    ]
    resources = [
      "${aws_elasticsearch_domain.main.arn}/*",
    ]
  }
}

resource "aws_iam_policy" "elasticsearch_domain_access" {
  name = "gitlab-${var.cluster_name}-elasticsearch_accesss"
  policy = data.aws_iam_policy_document.elasticsearch_domain_access.json
}
