# This file defines S3 buckets for an individual GitLab cluster.
#
# GitLab stores various files in S3 buckets. While it is possible
# to share a bucket, we have chosen to use separate buckets for
# each storage primitive so that we have better observability of
# how storage is utilized.

# Bucket namess are universal and have a convention of a suffix.

module "s3_bucket_artifacts" {
  source = "./s3_bucket"
  bucket = "company-gitlab-${var.cluster_name}-artifacts${var.s3_bucket_suffix}"
  region = var.aws_region
  cluster_name = var.cluster_name
  logging_bucket = var.s3_logging_bucket
  inventory_bucket_arn = var.s3_inventory_bucket_arn
  users_account_id = var.aws_account_id_users
}

module "s3_bucket_external_diffs" {
  source = "./s3_bucket"
  bucket = "company-gitlab-${var.cluster_name}-diffs${var.s3_bucket_suffix}"
  region = var.aws_region
  cluster_name = var.cluster_name
  logging_bucket = var.s3_logging_bucket
  inventory_bucket_arn = var.s3_inventory_bucket_arn
  users_account_id = var.aws_account_id_users
}

module "s3_bucket_lfs" {
  source = "./s3_bucket"
  bucket = "company-gitlab-${var.cluster_name}-lfs${var.s3_bucket_suffix}"
  region = var.aws_region
  cluster_name = var.cluster_name
  logging_bucket = var.s3_logging_bucket
  inventory_bucket_arn = var.s3_inventory_bucket_arn
  users_account_id = var.aws_account_id_users
}

module "s3_bucket_registry" {
  source = "./s3_bucket"
  bucket = "company-gitlab-${var.cluster_name}-registry${var.s3_bucket_suffix}"
  region = var.aws_region
  cluster_name = var.cluster_name
  logging_bucket = var.s3_logging_bucket
  inventory_bucket_arn = var.s3_inventory_bucket_arn
  users_account_id = var.aws_account_id_users
}

module "s3_bucket_uploads" {
  source = "./s3_bucket"
  bucket = "company-gitlab-${var.cluster_name}-uploads${var.s3_bucket_suffix}"
  region = var.aws_region
  cluster_name = var.cluster_name
  logging_bucket = var.s3_logging_bucket
  inventory_bucket_arn = var.s3_inventory_bucket_arn
  users_account_id = var.aws_account_id_users
}

data "aws_iam_policy_document" "s3_storage_bucket_read_write" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListBucket*",
    ]
    resources = [
      module.s3_bucket_artifacts.arn,
      module.s3_bucket_external_diffs.arn,
      module.s3_bucket_lfs.arn,
      module.s3_bucket_registry.arn,
      module.s3_bucket_uploads.arn,
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:DeleteObject*",
      "s3:GetObject*",
      "s3:PutObject*",
    ]
    resources = [
      "${module.s3_bucket_artifacts.arn}/*",
      "${module.s3_bucket_external_diffs.arn}/*",
      "${module.s3_bucket_lfs.arn}/*",
      "${module.s3_bucket_registry.arn}/*",
      "${module.s3_bucket_uploads.arn}/*",
    ]
  }
}

resource "aws_iam_policy" "s3_bucket_read_write" {
  name = "gitlab-${var.cluster_name}-s3_storage_read_write"
  description = "Grants read-write access for S3 buckets used by GitLab"
  policy = data.aws_iam_policy_document.s3_storage_bucket_read_write.json
}
