# Policy that allows read-only access to resources needed to
# run Ansible.
data "aws_iam_policy_document" "ansible_read" {
  # ListBucket needed by `awscli s3`.
  statement {
    effect = "Allow"
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      local.cluster_state_bucket_arn
    ]
  }
  # Allow read-only access to ansible playbook/role files for this
  # cluster.
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "${local.cluster_state_arn_prefix}/ansible/*",
    ]
  }
  # Allow read-only access to per-instance variable files for this
  # cluster.
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "${local.cluster_state_arn_prefix}/ansible-instance-variables/*",
    ]
  }
  # Allow fetching of Debian package from bootstrap bucket.
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "${local.bootstrap_bucket_arn}/${var.bootstrap_bucket_prefix}${var.gitlab_debian_package["path"]}"
    ]
  }
}

resource "aws_iam_policy" "ansible" {
  name = "gitlab-${var.cluster_name}-ansible_access"
  description = "Provides access to files used by Ansible for cluster ${var.cluster_name}"
  policy = data.aws_iam_policy_document.ansible_read.json
}
