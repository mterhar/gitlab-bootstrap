# Building Cluster Bootstrap Files

Bootstrapping a GitLab cluster requires various files to exist in an
S3 bucket. While these files should always be present in one of the
canonical S3 buckets (`company-gitlab-bootstrap` and
`company-gitlab-bootstrap-backup`), in the case of a catastrophic
disaster (unlikely) or corruption (likely), these files will need
to be seeded.

## Uploading Files from the `gitlab-bootstrap` Project

The repository containing this documentation contains a
`gitlab-bootstrap` directory defining the code behind
cluster bootstrapping.

There are various files in this directory that should be in
sync with the files in the S3 bucket.

To upload these files to an S3 bucket, simply run the
`upload-bootstrap` sub-command of `bootstrap.py`:

   $ python3.8 bootstrap.py upload-bootstrap

For testing purposes, you can use a different S3 bucket:

   $ python3.8 bootstrap.py --bootstrap-s3-bucket my-bucket upload-bootstrap

## Uploading a GitLab Omnibus Package

The bootstrap S3 bucket must contain a GitLab Omnibus package.

This package is typically produced and uploaded to S3 by regular
CI processes running against this repository. However, if the
content of the S3 bucket is lost or unavailable, you may need
to build and upload your own GitLab Omnibus package manually.

TODO document this process once we have the Omnibus build project
checked into this repository.
