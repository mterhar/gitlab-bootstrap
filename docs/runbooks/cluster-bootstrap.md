# Bootstrapping a GitLab Cluster

## Requirements

You will need the following to bootstrap a GitLab Cluster:

* A working Python 3.8+ interpreter.
* An AWS account with various permissions.
* The `awscli` tool or access to the AWS console.

The AWS account used to initiate a bootstrap requires the
following access:

* Read access to the entirety of the S3 bucket containing
  GitLab bootstrap files.
* Ability to create IAM roles, policies, and instance profiles.
* Ability to launch an EC2 instance.
* Read-only access to various EC2 APIs to discover AMIs, VPCs,
  subnets, etc.

## The GitLab Bootstrap S3 Bucket

An S3 bucket contains files and global state necessary for
bootstrapping GitLab clusters.

The primary S3 bucket is `company-gitlab-bootstrap`. The backup
bucket in another region is `company-gitlab-bootstrap-backup`.

If neither bucket is available, follow the instructions in
[Building Cluster Bootstrap Files](./building-cluster-bootstrap.md)
to seed a new S3 bucket.

## Obtaining the Bootstrap Script

The process of bootstrapping a GitLab cluster is automated
to the maximum extent possible and is driven by the
`bootstrap.py` Python script.

This script is defined in the `gitlab-bootstrap` directory of the
same repository containing this documentation.

A copy of the latest version of the `bootstrap.py` script is
available on the bootstrap S3 bucket and can be fetched as
follows:

    $ aws s3 cp s3://company-gitlab-bootstrap/bootstrap.py .

If you have a copy of this repository, you have a copy of
`bootstrap.py` in this repository which can also be used.

## Initiating a Bootstrap

To initiate a cluster bootstrap, simply run:

    $ python3.8 bootstrap.py bootstrap

The script will prompt you for missing information as needed.

If you would like to avoid the prompting, run
`python3.8 bootstrap.py --help` to see available options and
then pass those in via arguments.
