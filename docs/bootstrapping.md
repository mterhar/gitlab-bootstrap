# Bootstrapping Clusters

This document describes how a GitLab cluster is bootstrapped.

## Disaster Recovery Considerations

Source control is a foundational infrastructure service. Many systems
rely on the existence of a working - or partially working - source
control service.

The foundational nature of source control has implications for
disaster recovery scenarios as there is a chicken-and-egg
*bootstrapping* problem. If disaster struck and we were bootstrapping
a data center from scratch, source control would be one of the
first services deployed. Since we'd be bootstrapping from a fresh
data center that doesn't yet have a working source control service,
we need to take care to avoid hard dependencies on source control
as part of that bootstrapping process.

In order to bootstrap source control without a circular dependency,
we:

* Cannot run Chef using standard mechanisms because it relies on the
  Chef repository being served from a Git server.
* Cannot run critical services from Kubernetes because bootstrapping
  Kubernetes depends on source control.
* Cannot use a Terraform Enterprise workspace backed by source control
  because it requires a working Git server.
* Cannot rely on various other <Company> services that cannot be
  bootstrapped until source control is available.

What this means in practice is:

* Bootstrapping source control can only rely on core, always-available
  AWS services (S3, EC2, etc) and extremely limited other <Company>
  services that would be available in a partially bootstrapped data
  center.
* It is possible to use Terraform, but it needs to be possible to
  run it outside the context of Terraform Enterprise.

## Global Bootstrap S3 Bucket

The bootstrapping tool relies on files in an S3 bucket to
help drive the bootstrap process. This global bootstrap state
exists to support the bootstrapping and operational management
of all GitLab clusters at <Company>. The content of this
bucket - along with an AWS Account and Python interpreter -
are the only resources needed to bootstrap a new GitLab
cluster.

The S3 bucket contains files in well-defined locations:

`bootstrap.py` - In the root directory there exists a Python
script that is used to initiate a cluster bootstrap.

## Cluster State S3 Bucket

There exists a separate S3 bucket holding per-cluster state.

The top-most virtual directory in this bucket is the cluster
name. e.g. `production`. Each cluster has read-write access to
the key prefix underneath its corresponding virtual directory.

In each cluster directory, there exist the following special
keys:

`terraform.tfstate` - The Terraform state file holding the
latest state of the cluster as managed by Terraform.

## Bootstrapping Mechanism

We have designed the cluster bootstrapping mechanism to be
as turnkey and foolproof as possible such that recovery
during a disaster is quicker. A secondary benefit of this
design is that it facilitates testing on ad-hoc cluster instances
since it is so easy to spin up and tear down a new cluster.

The bootstrapping mechanism can be initiated with either:

1. A populated S3 bucket containing the files needed to
   initiate a bootstrap.
2. A copy of the repository containing this documentation,
   which has all the code used during bootstrap.

The heart of cluster bootstrapping is the `bootstrap.py`
Python script. When `bootstrap.py bootstrap` is executed,
the script will:

1. Locate support files (either from a zip file embedded in the
   file if using the file uploaded to the S3 bucket or from
   files in the repository if running locally).
1. Create a Python virtualenv in the `venv` directory and
   populate it with various packages (`boto3` for AWS API
   interaction, `paramiko` for a Python SSH client, etc).
1. Provision a cluster-specific IAM role and instance profile
   used for performing the bootstrap. The role has an inline
   policy that gives the role just enough permissions to
   initiate the bootstrap.
1. Launch a small EC2 instance in the target region using a
   generic Ubuntu AMI. The EC2 instance uses an IAM instance
   profile associated with the IAM role crated above. (The
   EC2 instance is terminated when the process exits or after
   a few hours.)
1. Connect to the SSH server on the new EC2 instance and
   upload a bootstrap script and various support files, including
   several Terraform files that define a GitLab cluster.
1. Execute the just-uploaded bootstrap script and stream the
   output to the local terminal.
1. Upon successful invocation of the bootstrap script on the
   temporary bootstrap instance, all Terraform/AWS resources
   are in place. EC2 instances have launched (or are in the
   process of launching) and have run or are running their
   respective `launch-<role>` Ansible playbooks.
1. Fetch the `admin_ip.txt` S3 object created by Terraform
   holding the IP address of the command-and-control *admin*
   EC2 instance.
1. Connect to the SSH server on the *admin* instance.

The bootstrap functionality performed by the script uploaded
to the ephemeral EC2 instance is as follows:

1. Installs various required system packages.
1. Downloads Terraform from Hashicorp's servers.
1. Runs `terraform init` to initialize the Terraform directory
   that was uploaded by `bootstrap.py`. This will connect to
   Terraform state stored in the *cluster state* S3 bucket.
1. `terraform import` global resources for the *bootstrap*
   and *cluster state* S3 buckets and the *bootstrap* IAM role.
1. `terraform plan` and `terraform apply` an additional IAM policy
   to the bootstrap role which gives the role sufficient
   permissions to perform subsequent activities.
1. Run `terraform plan` and `terraform apply` to instantiate all
   AWS resources required by the GitLab cluster.

## Bootstrapping EC2 Instances

A GitLab cluster consists of various components living on EC2
instances. The process for bootstrapping a new EC2 instance is
similar, regardless of the component it is providing.

All EC2 instances are launched with a common user data shell
script. The role of this shell script is to run an Ansible
playbook, which performs the heavy lifting of configuring the
instance. This shell script will:

1. Install global SSH authorized keys for the root user.
1. Install Ansible and the `awscli` tools.
2. Synchronize Ansible files from the cluster state S3 bucket to
   the local filesystem.
3. Download a JSON file in the cluster state S3 bucket named after
   the instance. e.g. `ansible-instance-variables/i-abcdef*.json`.
   It retries multiple times until the object becomes available.
4. Runs `ansible-playbook` using the downloaded variables against
   the `launch-*` playbook relevant to the instance's role.

The `launch-*` playbook does most of the work for configuring
instances, including downloading and installing the GitLab
Omnibus package, installing a `gitlab.rb` file, and starting
services.
