# Architecture

This document describes the deployment architecture of GitLab at
<Company>.

## Operating System Environment

GitLab EC2 instances run Ubuntu 20.04. (We would prefer to run
Amazon Linux 2 but it is
[not yet certified](https://gitlab.com/groups/gitlab-org/-/epics/2195)
by GitLab.)

We launch EC2 instances from AMIs published by the Ubuntu project
(https://cloud-images.ubuntu.com). (We do not rely on custom AMIs
because this complicates disaster recovery.)

GitLab application functionality is provided by a monolithic package
called [GitLab Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab).
This package provides all the files and operating system services
for running all of the various GitLab components (with the exception
of CI Runners, which aren't considered part of the core source control
service).

## GitLab Components

GitLab is composed of several logical components. See
[GitLab Architecture Overview](https://docs.gitlab.com/ee/development/architecture.html)
for the canonical source of these components.

While it is possible to run every service from a single server
(the GitHub Enterprise approach), this is not scalable nor does
it have the resiliency properties that we desire from a
foundational infrastructure service. 

<Company> runs GitLab in a high-availability configuration. This means
several components are operated on infrastructure dedicated to running
just a single component and various components consist of multiple
discrete EC2 instances for scalability and redundancy.

Our architecture is modeled after the architecture described in GitLab's
[high-available](https://docs.gitlab.com/ee/administration/high_availability/README.html)
and [AWS installation](https://docs.gitlab.com/ee/install/aws/)
documentation.

The sections below describe the components of a GitLab cluster.

### PostgreSQL on RDS

GitLab uses PostgreSQL for its primary relational database storage.
We are using managed PostgreSQL via RDS. We are leveraging the
high-availability features of RDS so there is resiliency against a
single AZ outage and automatic failover in case of problems with
the primary.

### Redis on ElasticCache

GitLab uses Redis for session data, caching, and job queues.

We are using managed Redis via the AWS ElastiCache service. Like
RDS, we run Redis across multiple AZs with auto-failover.

### S3 Buckets

GitLab requires blob storage for various categories of data.

We are using S3 buckets to facilitate this storage.

Our GitLab clusters utilize multiple S3 buckets so objects are
more strongly segmented by their type. We run separate S3 buckets
for:

* GitLab cluster bootstrapping (shared across clusters)
* Cluster configuration state
* TODO determine complete list (CI artifacts, registry, etc)

### Consul

GitLab uses Hashicorp Consul for service discovery.

We run a Consul cluster composed of 3 EC2 instances spread
across AZs. We need to run 3 instances so we can suffer the loss
of a single instance while still attaining quorum on consensus.

### Prometheus / Grafana

GitLab uses Prometheus for monitoring and Grafana for visualizing
metrics.

While <Company> is utilizing Datadog Agent for sending GitLab metrics
to Datadog (Datadog Agent has awareness of GitLab's built-in
metrics), you still need to run Prometheus somewhere for GitLab
to work.

Rather than running a separate Prometheus instance on each EC2
instance, we run a single EC2 instance dedicated to Prometheus /
Grafana. Monitoring for all EC2 instances are reported to a single
location, greatly increasing the utility when viewing the data.

### Gitaly

Gitaly is GitLab's Git service. This service handles storage of
Git repositories and provides a gRPC based server for interacting
with Git repositories.

There are various options for Gitaly's deployment architecture and
we have not yet decided which we will use.

### Elasticsearch

GitLab uses Elasticsearch to power search and other indexing
functionality.

We use Amazon Elasticsearch Service to provide Elasticsearch.

### Sidekiq

GitLab uses Sidekiq for background, asynchronous job execution.

We operate EC2 instances dedicated to Sidekiq. The EC2 instances
are in an auto scaling group and the number of instances dynamically
adjusts based on load.

### GitLab Rails

GitLab Rails is the component of GitLab that services HTTPS and
SSH traffic. It serves the main GitLab Rails application, which
powers the web site and API. It also terminates incoming SSH
traffic and proxies Git traffic to Gitaly.

We operate EC2 instances dedicated to GitLab Rails. The EC2 instances
are in an auto scaling group and the number of instances dynamically
adjusts based on load.

The EC2 instances are fronted by a Classic Load Balancer, which
sends HTTPS and SSH traffic to healthy EC2 instances.

### Command and Control Server

While not an official GitLab component, we leverage a single command
and control server to help manage each GitLab cluster. This server
facilitates the following activities:

* Bootstrapping a new cluster
* Coordinating GitLab upgrades
* Performing backups and restores
* Supplemental monitoring

There exists a single EC2 instance providing the command and
control server.
