# This playbook is called during cluster bootstrap to perform
# initial cluster configuration.
---
- hosts: localhost
  tasks:
    - name: Ensure admin instance is up to date
      import_role:
        name: gitlab-admin

    # We need to register an optional extension with PostgreSQL.
    - name: Register pg_trgm extension
      shell: "echo 'CREATE EXTENSION pg_trgm;' | /usr/bin/psql"
      environment:
        PGHOST: "{{ postgres_primary_address }}"
        PGPOST: "{{ postgres_primary_port }}"
        PGUSER: "{{ postgres_primary_username }}"
        PGPASSWORD: "{{ postgres_primary_password }}"
        PGDATABASE: "{{ postgres_primary_db_name }}"

    - name: Configure main database
      command: /opt/gitlab/bin/gitlab-rake gitlab:db:configure
      environment:
        # This environment variable is needed to configure the initial
        # root password on first run. Alternatively, you could set
        # gitlab_rails['initial_root_password'] and run
        # `gitlab-ctl reconfigure`. We do it this way so we don't have
        # a plaintext password in a config file.
        GITLAB_ROOT_PASSWORD: "{{ initial_root_password }}"

    - name: Configure praefect database
      command: /opt/gitlab/embedded/bin/praefect -config /var/opt/gitlab/praefect/config.toml sql-migrate

    - name: Upload gitlab-secrets.json file
      command: aws s3 cp /etc/gitlab/gitlab-secrets.json {{ cluster_state_s3_url }}/ansible/gitlab-secrets.json

    # This unblocks other EC2 instances from launching.
    - name: Upload cluster configured semaphore file
      command: aws s3 cp /etc/user_data_finished {{ cluster_state_s3_url }}/ansible/cluster_ready_semaphore

    - name: Run configure_settings.py Script
      command: /root/venv-configure/bin/python3 /root/configure_settings.py
      # This can fail on first run or due to missing admin token.
      # That's fine.
      ignore_errors: true

    - name: Determine if elasticsearch index was created
      stat:
        path: /etc/elasticsearch_index_created
      register: es_index_created_stat

    - name: Create ElasticSearch index
      command: /opt/gitlab/bin/gitlab-rake gitlab:elastic:create_empty_index
      when: es_index_created_stat.stat.exists == false
      register: es_index_created
      ignore_errors: true

    - name: Touch elasticsearch index created file
      file:
        path: /etc/elasticsearch_index_created
        state: touch
        owner: root
        group: root
        mode: '0644'
      when: es_index_created_stat.stat.exists == false and es_index_created.failed == false

    # These can all fail if the cluster isn't fully initialized yet.
    - name: Disable lfs_check feature
      command: /usr/bin/gitlab-rails runner 'Feature.disable(:lfs_check)'
      ignore_errors: true

    - name: Enable GitLab features
      command: /usr/bin/gitlab-rails runner 'Feature.enable({{ item }})'
      with_items:
        - ':ci_enable_live_trace'
        - ':junit_pipeline_view'
        - ':gitaly_upload_pack_filter'
      ignore_errors: true
