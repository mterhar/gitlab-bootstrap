---
- name: Apply global apt config
  copy:
    src: apt.conf
    dest: /etc/apt/apt.conf.d/99gitlab
    owner: root
    group: root
    mode: '0644'

- name: Ensure /etc/gitlab exists
  file:
    path: /etc/gitlab
    state: directory
    owner: root
    group: root
    mode: '0775'

- name: Ensure package install doesn't perform migrations
  file:
    path: /etc/gitlab/skip-auto-reconfigure
    state: touch
    owner: root
    group: root
    mode: '0644'

- name: Install gitlab-secrets.json
  command: aws s3 cp {{ cluster_state_s3_url }}/ansible/gitlab-secrets.json /etc/gitlab/gitlab-secrets.json
  when: skip_gitlab_secrets_download != true

- name: Install GitLab license
  copy:
    dest: /etc/gitlab/Gitlab.gitlab-license
    content: "{{ gitlab_license }}"
    owner: root
    group: root
    mode: '0644'
  when: gitlab_license != false

- name: Look for existing Debian package
  stat:
    path: /root/gitlab.deb
    get_checksum: yes
    checksum_algorithm: sha256
  register: existing_deb_stat

- name: Download Debian package
  command: aws s3 cp {{ gitlab_debian_package_s3_url }} /root/gitlab.deb
  when: existing_deb_stat.stat.exists == false or existing_deb_stat.stat.checksum != gitlab_debian_package_sha256

- name: Get SHA-256 of package
  stat:
    path: /root/gitlab.deb
    checksum_algorithm: sha256
    get_checksum: yes
  register: deb_stat

- name: Verify SHA-256 of GitLab Omnibus package
  fail:
    msg: "SHA-256 verification failed"
  when: deb_stat.stat.checksum != gitlab_debian_package_sha256

- name: Obtain version of downloaded GitLab Omnibus package
  command: dpkg -f /root/gitlab.deb Version
  register: deb_downloaded_version

- name: Check version of installed GitLab Omnibus package
  shell: dpkg -s gitlab-ee | grep '^Version:' | awk '{print $2}'
  ignore_errors: yes
  register: deb_installed_version

- name: Install GitLab Debian package
  command: dpkg -i /root/gitlab.deb
  # Only if it isn't installed or the installed version doesn't
  # match the current config.
  when: deb_installed_version.failed == true or deb_downloaded_version.stdout != deb_installed_version.stdout

# We do NOT trigger gitlab-ctl reconfigure here because we
# only want this role to install the package, not do anything
# more advanced. Orchestrating post-install activities across
# the cluster requires role-specific awareness.

