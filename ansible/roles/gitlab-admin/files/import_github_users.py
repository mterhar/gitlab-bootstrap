#!/usr/bin/env python3

import json
import pathlib
import random
import string

import github
import gitlab
import gitlab.exceptions
import ldap
import yaml

INSTANCE_VARIABLES = pathlib.Path("/etc/ansible_instance_vars.json")
EXTRA_VARIABLES = pathlib.Path("/etc/ansible_extra_instance_variables.yml")


def get_github_users(gh):
    users = {}

    for user in gh.get_users():
        users[user.login] = user

    return users


def get_gitlab_users(gl):
    gitlab_users = {}

    for u in gl.users.list(all=True):
        gitlab_users[u.attributes["username"]] = {
            "id": u.attributes["id"],
            "username": u.attributes["username"],
            "email": u.attributes["email"],
            "name": u.attributes["name"],
            "identities": u.attributes["identities"],
            "user": u,
        }

    return gitlab_users


def create_gitlab_user(gl, ldap_conn, username, gh_user):
    if gh_user.type.lower() != "user":
        print("skipping non-user %s" % gh_user.login)
        return

    email = None
    name = None

    if "ldap_dn" in gh_user._rawData:
        try:
            ldap_users = ldap_conn.search_ext_s(
                gh_user._rawData["ldap_dn"],
                ldap.SCOPE_BASE,
                attrlist=["cn", "uid", "mail"],
            )
        except ldap.NO_SUCH_OBJECT:
            ldap_users = []

        if ldap_users:
            attrs = ldap_users[0][1]

            if "mail" in attrs:
                email = attrs["mail"][0].decode("utf-8")
            if "cn" in attrs:
                name = attrs["cn"][0].decode("utf-8")

    if name is None:
        name = gh_user.name or gh_user.login

    if email is None:
        email = gh_user.email or "%s@github.vendor.com" % gh_user.login

    print("creating GitLab user %s" % username)

    return gl.users.create(
        {
            "username": username,
            "email": email,
            "name": name,
            "skip_confirmation": True,
            "password": "".join(
                [random.choice(string.ascii_letters + string.digits) for _ in range(32)]
            ),
        }
    )


def synchronize_users(gl, ldap_host, ldap_bind_dn, ldap_bind_password, github_users):
    gitlab_users = get_gitlab_users(gl)

    print(
        "synchronizing %d github users over %d gitlab users"
        % (len(github_users), len(gitlab_users))
    )

    ldap_conn = ldap.initialize("ldaps://%s:636" % ldap_host, bytes_mode=False)
    ldap_conn.bind(ldap_bind_dn, ldap_bind_password)

    for gh_username, entry in sorted(github_users.items()):
        # GitLab doesn't allow a user named `import`.
        if gh_username == "import":
            gitlab_username = "github-import"
        else:
            gitlab_username = gh_username

        if gitlab_username in gitlab_users:
            user = gitlab_users[gitlab_username]["user"]
            identities = {
                x["provider"]: x["extern_uid"]
                for x in gitlab_users[gitlab_username]["identities"]
            }

        else:
            try:
                user = create_gitlab_user(gl, ldap_conn, gitlab_username, entry)
                identities = {}
            except gitlab.exceptions.GitlabCreateError:
                user = None
                identities = {}

        if not user:
            continue

        if "ldap_dn" in entry._rawData:
            ldap_dn = entry._rawData["ldap_dn"]

            if identities.get("ldapmain") != ldap_dn:
                print("linking %s to LDAP user %s" % (user.username, ldap_dn))
                user.provider = "ldapmain"
                user.extern_uid = ldap_dn
                user.save()

        if identities.get("github") != gh_username:
            print("linking %s to GitHub user %s" % (user.username, gh_username))
            user.provider = "github"
            user.extern_uid = gh_username
            user.save()


def main():
    ansible_vars = {}

    with INSTANCE_VARIABLES.open("rb") as fh:
        ansible_vars.update(json.load(fh))

    with EXTRA_VARIABLES.open("rb") as fh:
        ansible_vars.update(yaml.load(fh, Loader=yaml.BaseLoader))

    gh = github.Github(
        base_url=ansible_vars["github_api_url"],
        login_or_token=ansible_vars["github_api_token"],
        user_agent="GitLab Bootstrapper",
    )

    gl = gitlab.Gitlab(
        ansible_vars["cluster_url"], private_token=ansible_vars["gitlab_admin_token"]
    )

    github_users = get_github_users(gh)
    synchronize_users(
        gl,
        ansible_vars["ldap_host"],
        ansible_vars["ldap_bind_dn"],
        ansible_vars["ldap_bind_password"],
        github_users,
    )


if __name__ == "__main__":
    main()
