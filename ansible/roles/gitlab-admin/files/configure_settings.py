#!/usr/bin/env python3

import json
import pathlib

import gitlab
import yaml

INSTANCE_VARIABLES = pathlib.Path("/etc/ansible_instance_vars.json")
EXTRA_VARIABLES = pathlib.Path("/etc/ansible_extra_instance_variables.yml")
LICENSE_PATH = pathlib.Path("/etc/gitlab/Gitlab.gitlab-license")
ES_INDEX_CREATED = pathlib.Path("/etc/elasticsearch_index_created")


def main():
    ansible_vars = {}

    with INSTANCE_VARIABLES.open("rb") as fh:
        ansible_vars.update(json.load(fh))

    with EXTRA_VARIABLES.open("rb") as fh:
        ansible_vars.update(yaml.load(fh, Loader=yaml.BaseLoader))

    if "gitlab_admin_token" not in ansible_vars:
        raise Exception("gitlab_admin_token not defined")

    gl = gitlab.Gitlab(
        ansible_vars["cluster_url"], private_token=ansible_vars["gitlab_admin_token"]
    )

    with LICENSE_PATH.open("r", encoding="ascii") as fh:
        license = fh.read()

    print("uploading GitLab license")
    gl.set_license(license)

    print("updating settings")
    settings = gl.settings.get()

    settings.allow_local_requests_from_system_hooks = True
    settings.allow_local_requests_from_web_hooks_and_services = True
    settings.default_group_visibility = "internal"
    settings.default_project_visibility = "internal"
    settings.default_snippet_visibility = "internal"
    settings.elasticsearch_url = "https://%s" % ansible_vars["elasticsearch_endpoint"]
    settings.elasticsearch_aws = True
    # It has to be set explicitly and its default value is whatever region we
    # are in, so use that.
    settings.elasticsearch_aws_region = settings.elasticsearch_aws_region

    # We can only enable elasticsearch indexing and searching if
    # the index is created.
    settings.elasticsearch_indexing = ES_INDEX_CREATED.exists()
    settings.elasticsearch_search = ES_INDEX_CREATED.exists()

    settings.enabled_git_access_protocol = "ssh"
    settings.hashed_storage_enabled = True
    settings.housekeeping_bitmaps_enabled = True
    settings.signup_enabled = False

    settings.save()


if __name__ == "__main__":
    main()
