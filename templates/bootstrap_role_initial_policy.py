# This Python file is evaluated early in bootstrap when populating
# the cluster bootstrap IAM role. This IAM role will be used by
# the bootstrap EC2 instance to initiate subsequent cluster bootstrap
# via Terraform.
#
# This policy provides the minimum permissions required to initiate
# the Terraform phase of the bootstrap.
#
# Having this policy defined as a Python file is a bit weird. But
# there is method to our madness!
#
# The bootstrap role and policy is eventually managed by Terraform.
# But it needs to be populated by Python code during bootstrap.py.
# This means both systems need to define the policy somewhere.
#
# Rather than define the policy in multiple places and risk things
# getting out of sync, we have decided to only define the policy in
# a single location.
#
# We could define the policy via a static JSON file. But then we
# couldn't have inline comments. And the content is dynamic, since
# we reference cluster-specific resource names.
#
# Other viable choices included using Terraform or a Jinja2 template.
# But we deemed these slightly more complex than using `exec()` from
# Python to evaluate the file as source code.
#
# See bootstrap.py for what symbols are available for reference in
# this file.

BOOTSTRAP_ROLE_INITIAL_POLICY = {
    "Version": "2012-10-17",
    "Statement": [
        # Grant read access to bucket-level metadata for bootstrap
        # bucket. This is needed to `terraform import` the bucket.
        {
            "Sid": "BootstrapBucketReadMetadata",
            "Effect": "Allow",
            "Action": ["s3:ListBucket", "s3:GetBucket*", "s3:Get*Configuration",],
            "Resource": [bootstrap_bucket_arn],
        },
        # Grant read access to bucket-level metadata for cluster
        # state bucket. This is needed to `terraform import` the bucket.
        {
            "Sid": "ClusterStateBucketReadMetadata",
            "Effect": "Allow",
            "Action": ["s3:ListBucket", "s3:GetBucket*", "s3:Get*Configuration",],
            "Resource": [cluster_state_bucket_arn],
        },
        # Grant read-write to our cluster state objects in the cluster
        # state bucket. This is needed to read/write Terraform state, etc.
        {
            "Sid": "ClusterStateBucketReadWriteOurData",
            "Effect": "Allow",
            "Action": ["s3:PutObject", "s3:GetObject"],
            "Resource": [
                "%s/%s/" % (cluster_state_bucket_arn, cluster_state_prefix),
                "%s/%s/*" % (cluster_state_bucket_arn, cluster_state_prefix),
            ],
        },
        # Allow to query current role. Used when importing the resource
        # into Terraform state.
        {
            "Sid": "RoleQuerySelf",
            "Effect": "Allow",
            "Action": ["iam:GetRolePolicy", "iam:GetRole"],
            "Resource": [
                "arn:aws:iam::%s:role/gitlab-%s-bootstrap"
                % (aws_account_id, cluster_name,)
            ],
        },
        # Allow to update role policies. This is used during bootstrap
        # to install additional policy grants such that further bootstrapping
        # can be performed.
        {
            "Sid": "RoleUpdateSelf",
            "Effect": "Allow",
            "Action": ["iam:PutRolePolicy"],
            "Resource": [
                "arn:aws:iam::%s:role/gitlab-%s-bootstrap"
                % (aws_account_id, cluster_name,)
            ],
        },
    ],
}
