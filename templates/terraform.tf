provider "aws" {
  region = "{{ aws_region }}"
}

provider "archive" {}

terraform {
  required_version = "~> 0.12.24"

  backend "s3" {
    region = "{{ cluster_state_bucket_region }}"
    bucket = "{{ cluster_state_bucket }}"
    key = "{{ terraform_state_key }}"
  }
}
