#!/bin/bash
# This script is run from an EC2 instance to bootstrap a GitLab
# cluster.
#
# This file is evaluated via Jinja2, so bar wary of variable expansion.

set -ex

TERRAFORM_URL="https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip"
TERRAFORM_SHA256="602d2529aafdaa0f605c06adb7c72cfb585d8aa19b3f4d8d189b42589e27bf11"

if [ -z "$1" ]; then
  echo "usage: $0 bootstrap|destroy"
  exit 1
fi

ACTION="$1"

cd /bootstrap

apt update
apt install -y --no-install-recommends \
  curl \
  unzip

# We validate the hash. So it is safe to ignore transport security.
curl --silent --show-error --insecure --location "${TERRAFORM_URL}" > terraform.zip
echo "${TERRAFORM_SHA256}  terraform.zip" | sha256sum --check
unzip -o terraform.zip

export PATH="/bootstrap:$PATH"

terraform init

terraform state list | grep module.cluster.aws_iam_role.bootstrap ||  \
  terraform import module.cluster.aws_iam_role.bootstrap gitlab-{{ cluster_name }}-bootstrap
terraform state list | grep module.cluster.aws_iam_role_policy.bootstrap_bootstrap || \
  terraform import module.cluster.aws_iam_role_policy.bootstrap_bootstrap gitlab-{{ cluster_name }}-bootstrap:bootstrap

if [ "$ACTION" = "bootstrap" ]; then
  # Once initial state is imported, kick off the bootstrap.
  # The first step is to add a new policy to the bootstrap role which
  # grants substantial additional AWS permissions.
  #
  # We perform this as an isolated step because we can't perform
  # a full plan without the new read grants in this policy.
  #
  # We don't define these grants in the initial role policy because
  # we want to minimize the size of that policy, since it is managed
  # in a somewhat hackily manner.
  terraform plan -target=module.cluster.aws_iam_role_policy.bootstrap_terraform -out=tfplan
  terraform apply -auto-approve tfplan

  # TODO IAM is eventually consistent. So there needs to be a delay or
  # some gating check here if the above `terraform apply` changed things,
  # otherwise actions below could fail due to a race against the IAM
  # policy taking effect.

  # Once new AWS policy grants are in play, we can continue with the
  # bootstrap.

  terraform plan -out tfplan
  terraform apply -auto-approve tfplan
elif [ "$ACTION" = "destroy" ]; then
  # When destroying clusters, we need to remove the bootstrap IAM role
  # resources from the state because if we destroyed them, we would
  # lose permissions to query and delete things midway through the
  # operation.
  terraform state rm module.cluster.aws_iam_role_policy.bootstrap_bootstrap || true
  terraform state rm module.cluster.aws_iam_role_policy.bootstrap_terraform || true
  terraform state rm module.cluster.aws_iam_role.bootstrap || true

  # TODO delete content of S3 buckets before attempting to delete them.
  # If we fail to do this, the bucket delete will fail.

  # Generate a plan to destroy everything and run it.
  terraform plan -destroy -out tfplan
  terraform apply -auto-approve tfplan
else
  echo "unknown action: ${ACTION}"
  exit 1
fi
