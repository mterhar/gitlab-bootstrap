# This block does all the magic of instantiating a GitLab
# cluster from parameters.
module "cluster" {
  source = "./terraform_modules/gitlab_cluster"
  aws_region = "{{ aws_region }}"
  bootstrap_bucket_region = "{{ bootstrap_bucket_region }}"
  bootstrap_bucket = "{{ bootstrap_bucket }}"
  bootstrap_bucket_prefix = "{{ bootstrap_bucket_prefix }}"
  cluster_state_bucket_region = "{{ cluster_state_bucket_region }}"
  cluster_state_bucket = "{{ cluster_state_bucket }}"
  cluster_state_bucket_prefix = "{{ cluster_state_bucket_prefix }}"
  cluster_name = "{{ cluster_name }}"
  bootstrap_role_initial_policy = file("bootstrap_role_initial_policy.json")
  vpc_id = "{{ vpc_id }}"
  route53_zone_id = "{{ route53_zone_id }}"
  route53_zone_name = "{{ route53_zone_name }}"
  security_groups = {
  }
  instance_ami = "{{ instance_ami }}"
  availability_zones = [
    {% for az in availability_zones -%}
    "{{ az }}",
    {%- endfor %}
  ]
  subnets_private = [
    {% for subnet in subnets_private -%}
    {
      id = "{{ subnet.id }}"
      availability_zone = "{{ subnet.availability_zone }}"
      cidr_block = "{{ subnet.cidr_block }}"
    },
    {%- endfor %}
  ]
  subnets_public = [
    {% for subnet in subnets_public -%}
    {
      id = "{{ subnet.id }}"
      availability_zone = "{{ subnet.availability_zone }}"
      cidr_block = "{{ subnet.cidr_block }}"
    },
    {%- endfor %}
  ]
  db_subnet_group_name = "{{ db_subnet_group_name }}"
  elasticache_subnet_group_name = "{{ elasticache_subnet_group_name }}"
  ssh_authorized_keys = file("ssh_authorized_keys")
}
