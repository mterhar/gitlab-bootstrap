#!/usr/bin/env python3

"""Self-contained script for bootstrapping a GitLab cluster.

This script requires extra support files to operate.

When run from a source checkout, those files are obtained
from the filesystem next to this file and added to a zip
file. That zip file is embedded in a copy of this script
then executed and the file content is loaded from the
in-source/in-memory zip file.

When this file is distributed, the zip file is embedded within
the distributed version so you don't need a full source checkout
to run.

The script is designed to be executed from a bare bones Python
installation. Upon invocation, it will create a virtualenv
in the `venv` directory and then reinvoke itself under that
Python environment.

See the Developer Portal documentation in this repository for
more on operation.
"""

# Only add `import` here for functionality that runs before
# virtualenv is activated.
import sys

if sys.version_info[0:2] < (3, 7):
    print("Python 3.7+ required")
    sys.exit(1)

import base64
import io
import os
import pathlib
import subprocess
import tempfile
import textwrap
import venv
import zipfile

# Add imports here for functionality needed after the
# virtualenv is activated.
try:
    import argparse
    import concurrent.futures
    import contextlib
    import hashlib
    import json
    import random
    import re
    import socket
    import tarfile
    import time

    import boto3
    import jinja2
    import paramiko
except ImportError:
    # We should only get here if the venv hasn't been initialized.
    # Failure in this scenario is fine, as we don't need these modules
    # before the venv activates.
    if "GITLAB_BOOTSTRAP_BOOTSTRAPPED" in os.environ:
        raise
    pass


DEFAULT_BOOTSTRAP_S3_BUCKET_REGION = "us-east-1"
DEFAULT_BOOTSTRAP_S3_BUCKET = "company-gitlab-bootstrap-dev"
DEFAULT_CLUSTER_STATE_S3_BUCKET_REGION = "us-east-1"
DEFAULT_CLUSTER_STATE_S3_BUCKET = "company-gitlab-cluster-state-dev"
DEFAULT_CLUSTER_REGION = "us-east-1"
DEFAULT_VPC_NAME = "dev-vpc-01"
DEFAULT_DNS_ZONE_NAME = "gitlab.dev.domain"

# The assume role policy document for the IAM role used to bootstrap
# a single cluster.
#
# The role is assumed by the bootstrapping EC2 instance.
BOOTSTRAP_ROLE_ASSUME_ROLE_POLICY = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {"Service": "ec2.amazonaws.com"},
            "Action": ["sts:AssumeRole"],
        }
    ],
}

HERE = pathlib.Path(os.path.abspath(__file__)).parent
VENV = HERE / "venv"


def add_common_cluster_arguments(parser):
    """Add command arguments common to interacting with a cluster."""
    parser.add_argument(
        "--cluster-state-s3-bucket-region",
        default=DEFAULT_CLUSTER_STATE_S3_BUCKET_REGION,
        help="AWS region for cluster state S3 bucket",
    )
    parser.add_argument(
        "--cluster-state-s3-bucket",
        default=DEFAULT_CLUSTER_STATE_S3_BUCKET,
        help="S3 bucket for holding cluster state",
    )
    parser.add_argument(
        "--cluster-state-s3-bucket-prefix",
        default="",
        help="S3 bucket prefix within --cluster-state-s3-bucket-prefix",
    )
    parser.add_argument("--cluster-name", help="Name of the cluster")
    parser.add_argument(
        "--cluster-region",
        default=DEFAULT_CLUSTER_REGION,
        help="AWS region for GitLab cluster",
    )
    parser.add_argument(
        "--extra-instance-variables",
        help="Path to a YAML file containing extra Ansible variables to make available",
    )


def main():
    """Runs main functionality in the context of an activated virtualenv."""
    parser = argparse.ArgumentParser(description="GitLab cluster bootstrapper")

    parser.add_argument(
        "--bootstrap-s3-bucket-region",
        default=DEFAULT_BOOTSTRAP_S3_BUCKET_REGION,
        help="AWS region for bootstrap S3 bucket",
    )
    parser.add_argument(
        "--bootstrap-s3-bucket",
        default=DEFAULT_BOOTSTRAP_S3_BUCKET,
        help="S3 bucket for managing GitLab cluster bootstraps",
    )
    parser.add_argument(
        "--bootstrap-s3-bucket-prefix",
        default="",
        help="S3 bucket prefix within --bootstrap-s3-bucket",
    )

    subparsers = parser.add_subparsers()

    bootstrap_parser = subparsers.add_parser(
        "bootstrap", help="Bootstrap a GitLab cluster"
    )
    bootstrap_parser.set_defaults(fn=command_bootstrap)
    add_common_cluster_arguments(bootstrap_parser)
    bootstrap_parser.add_argument(
        "--ami", help="EC2 Image AMI to use for new instances"
    )
    bootstrap_parser.add_argument(
        "--vpc-name", default=DEFAULT_VPC_NAME, help="Name of VPC to put resources in"
    )
    bootstrap_parser.add_argument(
        "--dns-zone-name",
        default=DEFAULT_DNS_ZONE_NAME,
        help="Name of DNS zone to place cluster into",
    )
    bootstrap_parser.add_argument(
        "--ignore-availability-zone",
        default=[],
        action="append",
        dest="ignore_availability_zones",
        help="Specify an availability zone to not place resources into",
    )

    configure_parser = subparsers.add_parser(
        "configure", help="Configure a GitLab cluster"
    )
    configure_parser.set_defaults(fn=command_configure)
    add_common_cluster_arguments(configure_parser)

    destroy_parser = subparsers.add_parser("destroy", help="Destroy a GitLab cluster")
    destroy_parser.set_defaults(fn=command_destroy)
    destroy_parser.add_argument(
        "--ignore-availability-zone",
        default=[],
        action="append",
        dest="ignore_availability_zones",
        help="Specify an availability zone to not place resources into",
    )
    destroy_parser.add_argument(
        "--vpc-name", default=DEFAULT_VPC_NAME, help="Name of VPC to put resources in"
    )
    add_common_cluster_arguments(destroy_parser)

    upload_bootstrap_parser = subparsers.add_parser(
        "upload-bootstrap", help="Upload bootstrap.py to bootstrap bucket"
    )
    upload_bootstrap_parser.set_defaults(fn=command_upload_bootstrap)

    args = parser.parse_args()

    if not hasattr(args, "fn"):
        parser.print_help()
        return

    kwargs = dict(vars(args))
    del kwargs["fn"]
    return args.fn(**kwargs)


def get_state_from_cluster_args(
    bootstrap_s3_bucket_region,
    bootstrap_s3_bucket,
    bootstrap_s3_bucket_prefix,
    cluster_state_s3_bucket_region,
    cluster_state_s3_bucket,
    cluster_state_s3_bucket_prefix,
    cluster_name,
    cluster_region,
    extra_instance_variables,
):
    if not cluster_name:
        while True:
            cluster_name = input("Name of cluster to configure: ").strip()
            if cluster_name:
                break

    if not re.match("^[a-z-]+$", cluster_name):
        raise Exception("Cluster names can only contain a-z and '-' characters")

    aws_session = boto3.session.Session()

    if extra_instance_variables:
        with open(extra_instance_variables, "rb") as fh:
            extra_instance_variables = fh.read()
    else:
        extra_instance_variables = b"---\n"

    return {
        "cluster_name": cluster_name,
        "cluster_region": cluster_region,
        "bootstrap_bucket_region": bootstrap_s3_bucket_region,
        "bootstrap_bucket": bootstrap_s3_bucket,
        "bootstrap_bucket_prefix": bootstrap_s3_bucket_prefix.strip("/"),
        "cluster_state_bucket_region": cluster_state_s3_bucket_region,
        "cluster_state_bucket": cluster_state_s3_bucket,
        "cluster_state_bucket_prefix": cluster_state_s3_bucket_prefix.strip("/"),
        "cluster_state_bucket_key_prefix": (
            "%s/%s" % (cluster_state_s3_bucket_prefix.strip("/"), cluster_name)
        ).lstrip("/"),
        "extra_instance_variables": extra_instance_variables,
        "aws_session": aws_session,
        "aws_account_id": aws_session.client("sts")
        .get_caller_identity()
        .get("Account"),
        "support_files": load_support_data(),
    }


def command_bootstrap(
    vpc_name, dns_zone_name, ignore_availability_zones, ami, **kwargs,
):
    """Implementation of the `bootstrap` command."""
    state = get_state_from_cluster_args(**kwargs)
    state["vpc_name"] = vpc_name

    cluster_name = state["cluster_name"]
    cluster_region = state["cluster_region"]
    aws_session = state["aws_session"]

    role = ensure_bootstrap_iam_role(state)

    if ami:
        image = aws_session.resource("ec2", region_name=cluster_region).Image(ami)
        image.load()
    else:
        image = find_ubuntu_ami(aws_session, region_name=cluster_region)

    print("using AMI %s: %s" % (image.id, image.name))
    state["instance_image"] = image

    state["networking"] = find_networking(
        aws_session,
        cluster_region,
        state["vpc_name"],
        ignore_availability_zones,
        cluster_name,
    )

    state["dns_zone"] = find_route53_zone(aws_session, cluster_region, dns_zone_name)

    # Ensure Ansible files are in cluster state S3 bucket before any
    # Terraform activity occurs.
    synchronize_cluster_ansible_files(state)

    # Launch a temporary EC2 instance to perform initial bootstrap.
    # This will perform a Terraform run and get all the managed AWS
    # resources in place.
    with launch_bootstrap_ec2_instance(state, role, image) as instance:
        print("waiting for SSH connection...")
        ssh = wait_for_ssh(
            instance.private_ip_address, 22, timeout=60.0, username="root"
        )
        print("got SSH connection to bootstrap EC2 instance")
        print("to connect: $ ssh -l root %s" % instance.private_ip_address)
        with ssh:
            bootstrap_exit = perform_cluster_bootstrap(state, ssh)

    if bootstrap_exit != 0:
        return bootstrap_exit

    # We continue bootstrap by connecting to the admin instance and
    # running Ansible from it.
    return perform_cluster_config(state)


def command_configure(**kwargs,):
    state = get_state_from_cluster_args(**kwargs)

    return perform_cluster_config(state)


def command_destroy(ignore_availability_zones, vpc_name, **kwargs):
    state = get_state_from_cluster_args(**kwargs)
    state["vpc_name"] = vpc_name

    role = ensure_bootstrap_iam_role(state)
    image = find_ubuntu_ami(state["aws_session"], region_name=state["cluster_region"])
    state["instance_image"] = image
    state["networking"] = find_networking(
        state["aws_session"],
        state["cluster_region"],
        state["vpc_name"],
        ignore_availability_zones,
        state["cluster_name"],
    )

    return destroy_cluster(state, role, image)


def command_upload_bootstrap(
    bootstrap_s3_bucket_region, bootstrap_s3_bucket, bootstrap_s3_bucket_prefix
):
    """Implementation of the `upload-bootstrap` command."""

    aws_session = boto3.session.Session()

    s3 = aws_session.resource("s3", region_name=bootstrap_s3_bucket_region)
    bucket = s3.Bucket(bootstrap_s3_bucket)

    key = ("%s/bootstrap.py" % bootstrap_s3_bucket_prefix).lstrip("/")
    obj = bucket.Object(key)

    print("uploading to s3://%s/%s" % (bucket.name, obj.key))

    # If we get here, we're should be running from a self-contained
    # script. So upload ourselves (after verifying).
    load_support_data()

    with open(__file__, "rb") as fh:
        obj.upload_fileobj(fh)


def ensure_bootstrap_iam_role(state):
    """Ensures the IAM role used by the bootstrapper exists.

    If it doesn't exist, it will be created. If it does exist, its
    policies will be reconciled with those defined in this file.
    """
    client = state["aws_session"].client("iam", region_name=state["cluster_region"])
    resource = state["aws_session"].resource("iam", region_name=state["cluster_region"])

    cluster_name = state["cluster_name"]

    role_name = "gitlab-%s-bootstrap" % cluster_name

    try:
        client.get_role(RoleName=role_name)
    except client.exceptions.NoSuchEntityException:
        print("creating IAM role: %s" % role_name)
        client.create_role(
            RoleName=role_name,
            Description="Used to bootstrap the %s GitLab cluster" % cluster_name,
            AssumeRolePolicyDocument=json.dumps(
                BOOTSTRAP_ROLE_ASSUME_ROLE_POLICY, sort_keys=True, indent=4
            ),
            Tags=[
                {"Key": "Name", "Value": "gitlab"},
                {"Key": "ManagedBy", "Value": "gitlab-bootstrap"},
                {"Key": "GitLabCluster", "Value": cluster_name},
            ],
        )

        waiter = client.get_waiter("role_exists")
        waiter.wait(RoleName=role_name)

    try:
        client.get_instance_profile(InstanceProfileName=role_name)
    except client.exceptions.NoSuchEntityException:
        print("creating IAM instance profile")
        resource.create_instance_profile(InstanceProfileName=role_name)

        # Instance profiles take a while to create. So wait on it.
        waiter = client.get_waiter("instance_profile_exists")
        waiter.wait(InstanceProfileName=role_name)

    role = resource.Role(role_name)

    # Associate instance profile with role.
    if not list(role.instance_profiles.all()):
        print("adding IAM instance profile to role")
        instance_profile = resource.InstanceProfile(role_name)
        instance_profile.add_role(RoleName=role_name)

    print("bootstrap role: %s" % role.arn)

    # Specify inline policies to facilitate bootstrapping.
    bootstrap_bucket_arn = (
        "arn:aws:s3:::%s/%s"
        % (state["bootstrap_bucket"], state["bootstrap_bucket_prefix"],)
    ).rstrip("/")
    cluster_state_bucket_arn = "arn:aws:s3:::%s" % state["cluster_state_bucket"]
    cluster_state_prefix = (
        "%s/%s" % (state["cluster_state_bucket_prefix"], cluster_name,)
    ).lstrip("/")

    # exec() allows us to execute source code with an arbitrary
    # globals and locals symbol table. The file shouldn't depend
    # on any Python symbols other than specific string variables.
    # So we populate just the variables we need and evaluate in
    # that context.
    print("evaluating bootstrap_role_initial_policy.py file")
    globs = {}
    locs = dict(
        bootstrap_bucket_arn=bootstrap_bucket_arn,
        cluster_state_bucket_arn=cluster_state_bucket_arn,
        cluster_state_prefix=cluster_state_prefix,
        aws_account_id=state["aws_account_id"],
        cluster_name=cluster_name,
    )
    exec(
        state["support_files"]["templates/bootstrap_role_initial_policy.py"].decode(
            "utf-8"
        ),
        globs,
        locs,
    )
    policy_data = locs["BOOTSTRAP_ROLE_INITIAL_POLICY"]

    policy = role.Policy("bootstrap")
    policy.put(PolicyDocument=json.dumps(policy_data, sort_keys=True, indent=4))

    # Also write out the policy JSON as a file available to Terraform.
    # Putting this in templates/ means it will get expanded again. But
    # this should be safe, as there is no template syntax. If we ever
    # have a directory with static files, we should put this file there
    # instead.
    state["support_files"]["templates/bootstrap_role_initial_policy.json"] = json.dumps(
        policy_data, sort_keys=True, indent=4
    ).encode("utf-8")

    return role


def delete_bootstrap_iam_role(aws_session, region_name, role_name):
    """Delete the bootstrap IAM role."""
    resource = aws_session.resource("iam", region_name=region_name)

    role = resource.Role(name=role_name)

    for instance_profile in role.instance_profiles.all():
        print(
            "removing instance profile %s from role %s"
            % (instance_profile.name, role.name)
        )
        instance_profile.remove_role(RoleName=role.name)
        instance_profile.delete()

    for policy in role.policies.all():
        print("removing inline policy %s from role %s" % (policy.name, role.name))
        policy.delete()

    print("deleting role %s" % role.name)
    role.delete()


def create_tar_from_files(files):
    """Creates data for a tar archive from file content.

    files is a dict of {filename: data}.
    """
    with io.BytesIO() as fh:
        with tarfile.open("", "w|", fileobj=fh) as tf:
            for filename, data in sorted(files.items()):
                # All these attributes aren't necessary. But they
                # should make the tar file deterministic, which is
                # always a nice property.
                ti = tarfile.TarInfo(filename)
                ti.mode = 0o0644
                ti.type = tarfile.REGTYPE
                ti.uid = 0
                ti.gid = 0
                ti.uname = ""
                ti.gname = ""
                ti.size = len(data)

                tf.addfile(ti, io.BytesIO(data))

        return fh.getvalue()


def find_ubuntu_ami(aws_session, region_name):
    """Finds the Ubuntu AMI to use for new EC2 instances."""
    ec2 = aws_session.resource("ec2", region_name=region_name)

    print("locating latest Ubuntu AMI...")

    candidates = list(
        ec2.images.filter(
            Filters=[
                # This is the canonical owner ID.
                {"Name": "owner-id", "Values": ["099720109477"]},
                {"Name": "state", "Values": ["available"]},
                {"Name": "image-type", "Values": ["machine"]},
                {"Name": "hypervisor", "Values": ["xen"]},
                {"Name": "virtualization-type", "Values": ["hvm"]},
                {"Name": "root-device-type", "Values": ["ebs"]},
                {
                    "Name": "name",
                    "Values": [
                        "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
                    ],
                },
            ]
        )
    )

    candidates.sort(key=lambda x: x.creation_date)

    return candidates[-1]


def find_networking(
    aws_session, region_name, vpc_name, ignore_availability_zones, cluster_name
):
    """Find networking info needed to launch EC2 instances."""
    ec2 = aws_session.resource("ec2", region_name=region_name)

    target_vpc = None

    for vpc in ec2.vpcs.all():
        for tag in vpc.tags:
            if tag["Key"] == "Name" and tag["Value"] == vpc_name:
                target_vpc = vpc
                break

    if not target_vpc:
        raise Exception("Could not find VPC: %s" % vpc_name)

    # There should be 1 public subnet and 2 private subnets per AZ.
    # The 2 private subnets are divided by IP ranges.

    res = {
        "vpc": target_vpc,
        "subnets": {"private": [], "public": [],},
        "security_groups": {},
    }

    for subnet in target_vpc.subnets.all():
        name = None
        for tag in subnet.tags:
            if tag["Key"] == "Name":
                name = tag["Value"]
                break

        if not name:
            continue

        if subnet.cidr_block.startswith("100."):
            continue

        # Filter out availability zones we are told to ignore. We need
        # this feature because we apparently do not support launching
        # EC2 instances in all AZs in all AWS accounts, even if a subnet
        # is available.
        if subnet.availability_zone in ignore_availability_zones:
            continue

        if "-private-" in name:
            res["subnets"]["private"].append(subnet)
        elif "-public-" in name:
            res["subnets"]["public"].append(subnet)

    if not res["subnets"]["private"] or not res["subnets"]["public"]:
        raise Exception("could not find subnets")

    # Now that we have gathered all subnets, we want to sort them so
    # behavior is deterministic (barring the introduction of a new
    # subnet/az of course).
    #
    # The logical way to sort would be by availability zone. But,
    # sorting by availability zone would mean the order would always
    # be the same across all clusters. If a component only needed to
    # use 3 AZs, we would always place resources in e.g. zones a, b,
    # and c. This isn't a very efficient use of resources.
    #
    # Instead, we do a "deterministically random" sort. We seed a
    # random number generator with the cluster name so it is
    # deterministic. Then, starting with a sorted list by
    # availability zone (which is deterministic), we do a list
    # shuffle using the deterministic random number generator. This
    # yields a shuffled list that is deterministic for each cluster.
    res["subnets"]["private"].sort(key=lambda x: x.availability_zone)
    res["subnets"]["public"].sort(key=lambda x: x.availability_zone)

    rng = random.Random(cluster_name)
    rng.shuffle(res["subnets"]["private"])
    rng.shuffle(res["subnets"]["public"])

    for sg in target_vpc.security_groups.all():
        name = None
        for tag in sg.tags or []:
            if tag["Key"] == "Name":
                name = tag["Value"]
                break

        if name:
            res["security_groups"][name] = sg

    # We also need to locate the shared RDS subnet group.
    rds = aws_session.client("rds", region_name=region_name)
    db_subnet_group_name = None
    for group in rds.describe_db_subnet_groups()["DBSubnetGroups"]:
        if group["DBSubnetGroupName"] == "%s-db-subnet-group" % vpc_name:
            db_subnet_group_name = group["DBSubnetGroupName"]
            break

    if not db_subnet_group_name:
        raise Exception("could not find DB subnet group for vpc %s" % vpc_name)

    res["db_subnet_group_name"] = db_subnet_group_name

    # Locate the shared ElastiCache subnet group.
    elasticache = aws_session.client("elasticache", region_name=region_name)
    elasticache_subnet_group_name = None
    for group in elasticache.describe_cache_subnet_groups()["CacheSubnetGroups"]:
        if group["CacheSubnetGroupName"] == "%s-elasticache-subnet-group" % vpc_name:
            elasticache_subnet_group_name = group["CacheSubnetGroupName"]
            break

    if not elasticache_subnet_group_name:
        raise Exception("could not find elasticache subnet group for vpc %s" % vpc_name)

    res["elasticache_subnet_group_name"] = elasticache_subnet_group_name

    return res


def find_route53_zone(aws_session, region_name, dns_zone_name):
    """Find the Route53 zone to place clusters into."""
    route53 = aws_session.client("route53", region_name=region_name)

    zones = route53.list_hosted_zones_by_name(DNSName=dns_zone_name, MaxItems="1")[
        "HostedZones"
    ]
    if not zones:
        raise Exception("could not find Route53 zone: %s" % dns_zone_name)

    zone = zones[0]

    return {
        "id": zone["Id"],
        "name": zone["Name"],
    }


def synchronize_cluster_ansible_files(state):
    """Synchronizes static Ansible files for a given cluster.

    This will upload all ansible/ files into the S3 cluster state
    bucket for a given cluster.
    """
    s3 = state["aws_session"].resource(
        "s3", region_name=state["cluster_state_bucket_region"]
    )
    bucket = s3.Bucket(state["cluster_state_bucket"])

    # We store Ansible files in a tar archive so they can be
    # fetched and mutated atomically. Otherwise there are race
    # conditions when syncing files.
    archive_files = {
        filename: data
        for filename, data in state["support_files"].items()
        if filename.startswith("ansible/")
    }
    archive_data = create_tar_from_files(archive_files)

    obj = bucket.Object(
        "%s/ansible/latest.tar" % state["cluster_state_bucket_key_prefix"]
    )
    print("uploading Ansible tar archive to %s" % obj.key)
    with io.BytesIO(archive_data) as fh:
        obj.upload_fileobj(fh)

    obj = bucket.Object(
        "%s/ansible/extra_instance_variables.yml"
        % state["cluster_state_bucket_key_prefix"]
    )
    print("uploading extra instance variables to %s" % obj.key)
    with io.BytesIO(state["extra_instance_variables"]) as fh:
        obj.upload_fileobj(fh)


@contextlib.contextmanager
def launch_bootstrap_ec2_instance(
    state, role, image, security_group="internal",
):
    aws_session = state["aws_session"]

    if security_group not in state["networking"]["security_groups"]:
        raise Exception("could not find security group %s" % security_group)

    user_data = expand_template(
        state["support_files"]["bootstrap_instance_user_data.sh"].decode("utf-8"), state
    )

    ec2 = aws_session.resource("ec2")

    print("launching bootstrap EC2 instance")
    try:
        instances = []
        instances = ec2.create_instances(
            ImageId=image.id,
            InstanceType="t3.small",
            MinCount=1,
            MaxCount=1,
            InstanceInitiatedShutdownBehavior="terminate",
            BlockDeviceMappings=[
                {
                    "DeviceName": image.block_device_mappings[0]["DeviceName"],
                    "Ebs": {
                        "DeleteOnTermination": True,
                        "VolumeSize": 8,
                        "VolumeType": "gp2",
                    },
                }
            ],
            IamInstanceProfile={"Name": role.name},
            SubnetId=state["networking"]["subnets"]["private"][0].id,
            SecurityGroupIds=[
                state["networking"]["security_groups"][security_group].id
            ],
            TagSpecifications=[
                {
                    "ResourceType": "instance",
                    "Tags": [
                        {"Key": "Name", "Value": "gitlab"},
                        {"Key": "GitLabCluster", "Value": state["cluster_name"]},
                        {"Key": "GitLabRole", "Value": "bootstrap"},
                    ],
                }
            ],
            UserData=user_data,
        )

        for instance in instances:
            print("waiting for %s to start running..." % instance.id)
            instance.wait_until_running()
            print("instance %s is running" % instance.id)

        yield instances[0]
    finally:
        for instance in instances:
            print("terminating %s" % instance.id)
            instance.terminate()


def perform_cluster_bootstrap(state, ssh):
    """Perform cluster bootstrap on an EC2 instance via an SSH client."""
    write_bootstrap_files(state, ssh)

    print("running bootstrap from EC2 instance")
    return run_command_ssh(
        ssh, "/usr/bin/bash /bootstrap/bootstrap_from_instance.sh bootstrap"
    )


def wait_for_ssh(
    hostname: str, port: int, timeout: int, username: str, poll_interval=5.0
):
    """Wait for an SSH server to accept logins on the specified hostname and port."""

    class IgnoreHostKeyPolicy(paramiko.MissingHostKeyPolicy):
        def missing_host_key(self, client, hostname, key):
            pass

    end_time = time.time() + timeout

    while True:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(IgnoreHostKeyPolicy())

        try:
            client.connect(
                hostname,
                port=port,
                timeout=poll_interval,
                username=username,
                allow_agent=True,
                look_for_keys=False,
            )
            return client
        except socket.error:
            print("... SSH socket not responding")
        except paramiko.SSHException:
            print("... SSH server not responding")
        except paramiko.AuthenticationException:
            raise

        if time.time() >= end_time:
            raise Exception("Timed out waiting for SSH")

        time.sleep(poll_interval)


def run_command_ssh(client, command):
    """Runs a command via SSH, prints output, and returns exit code."""
    channel = client.get_transport().open_session()
    channel.exec_command(command)

    channel.set_combine_stderr(True)

    stdin = channel.makefile("wb", -1)
    stdout = channel.makefile("r", -1)
    stdin.close()

    for line in stdout:
        print(line, end="")

    return channel.recv_exit_status()


def write_bootstrap_files(
    state, ssh_client,
):
    """Write files to bootstrap a cluster via an SFTP client."""
    upload_files = {}

    for path, data in state["support_files"].items():
        if path.startswith("terraform_modules/"):
            upload_path = path
        elif path.startswith("templates/"):
            data = expand_template(data.decode("utf-8"), state).encode("utf-8")
            upload_path = path[len("templates/") :]
        else:
            continue

        upload_files[upload_path] = data

    print("uploading bootstrap files")
    sftp = ssh_client.open_sftp()
    sftp.mkdir("/bootstrap")
    with sftp.open("/bootstrap/bootstrap.tar", "wb") as fh:
        fh.write(create_tar_from_files(upload_files))

    if run_command_ssh(
        ssh_client, "/usr/bin/tar -C /bootstrap -xvf /bootstrap/bootstrap.tar",
    ):
        raise Exception("error extracting tar archive")


def expand_template(template: str, state):
    """Expand a jinja2 template string."""

    bootstrap_bucket_prefix = state["bootstrap_bucket_prefix"]
    if bootstrap_bucket_prefix:
        bootstrap_bucket_prefix += "/"

    cluster_state_bucket_prefix = state["cluster_state_bucket_prefix"]
    if cluster_state_bucket_prefix:
        cluster_state_bucket_prefix += "/"

    security_groups = {}
    for name, sg in state["networking"]["security_groups"].items():
        security_groups[name] = sg.id

    subnets_private = []
    for subnet in state["networking"]["subnets"]["private"]:
        subnets_private.append(
            {
                "id": subnet.id,
                "availability_zone": subnet.availability_zone,
                "cidr_block": subnet.cidr_block,
            }
        )
    subnets_public = []
    for subnet in state["networking"]["subnets"]["public"]:
        subnets_public.append(
            {
                "id": subnet.id,
                "availability_zone": subnet.availability_zone,
                "cidr_block": subnet.cidr_block,
            }
        )

    args = dict(
        aws_region=state["cluster_region"],
        ssh_authorized_keys=state["support_files"]["ssh_authorized_keys"].decode(
            "ascii"
        ),
        terraform_state_key=(
            "%s/%s/terraform.tfstate"
            % (state["cluster_state_bucket_prefix"], state["cluster_name"],)
        ).lstrip("/"),
        bootstrap_bucket_region=state["bootstrap_bucket_region"],
        bootstrap_bucket=state["bootstrap_bucket"],
        bootstrap_bucket_prefix=bootstrap_bucket_prefix,
        cluster_state_bucket_region=state["cluster_state_bucket_region"],
        cluster_state_bucket=state["cluster_state_bucket"],
        cluster_state_bucket_prefix=cluster_state_bucket_prefix,
        cluster_name=state["cluster_name"],
        instance_ami=state["instance_image"].id,
        availability_zones=[
            x.availability_zone for x in state["networking"]["subnets"]["private"]
        ],
        subnets_private=subnets_private,
        subnets_public=subnets_public,
        vpc_id=state["networking"]["vpc"].id,
        security_groups=security_groups,
        db_subnet_group_name=state["networking"]["db_subnet_group_name"],
        elasticache_subnet_group_name=state["networking"][
            "elasticache_subnet_group_name"
        ],
    )

    if "dns_zone" in state:
        args.update(
            dict(
                route53_zone_id=state["dns_zone"]["id"],
                route53_zone_name=state["dns_zone"]["name"],
            )
        )

    return jinja2.Template(template).render(**args)


def perform_cluster_config(state):
    """Performs cluster configuration post Terraform bootstrap."""
    print("performing cluster configuration")

    synchronize_cluster_ansible_files(state)

    # We discover the IP address of the admin instance by querying
    # an S3 object.
    s3 = state["aws_session"].resource(
        "s3", region_name=state["cluster_state_bucket_region"]
    )
    bucket = s3.Bucket(state["cluster_state_bucket"])
    admin_ip_key = "%s/admin_ip.txt" % state["cluster_state_bucket_key_prefix"]
    obj = bucket.Object(admin_ip_key)
    print("fetching admin instance IP from s3://%s/%s" % (bucket.name, obj.key))

    with io.BytesIO() as fh:
        obj.download_fileobj(fh)

        admin_ip = fh.getvalue().decode("ascii")
        print("resolved admin IP to %s" % admin_ip)

    print("attempting to connect to admin instance...")
    with wait_for_ssh(admin_ip, 22, timeout=60.0, username="root") as ssh:
        # Wait for the launch playbook to finish, otherwise we have
        # race conditions.
        print("ensuring user data script has finished...")
        sftp = ssh.open_sftp()
        while True:
            try:
                sftp.stat("/etc/user_data_finished")
                break
            except FileNotFoundError:
                print("... still waiting")
                time.sleep(5.0)

        commands = [
            "/root/sync-ansible",
            "ansible-playbook -e @/etc/ansible_instance_vars.json /root/ansible/cluster-bootstrap.yml",
        ]

        return run_command_ssh(ssh, " && ".join(commands))


def find_cluster_s3_buckets(state):
    """Find S3 buckets belonging to a specific cluster."""
    s3 = state["aws_session"].resource(
        "s3", region_name=state["cluster_state_bucket_region"]
    )

    return [
        bucket
        for bucket in s3.buckets.all()
        if bucket.name.startswith("company-gitlab-%s" % state["cluster_name"])
    ]


def remove_s3_bucket_objects(bucket, prefix=None):
    """Remove objects in an S3 bucket."""

    # We have to operate on `object_versions` because old versions of
    # an object prevent bucket deletion.
    if prefix:
        objects = bucket.object_versions.filter(Prefix=prefix)
    else:
        objects = bucket.object_versions.all()

    batch = []

    with concurrent.futures.ThreadPoolExecutor(8) as e:
        for obj in objects:
            batch.append({"Key": obj.key, "VersionId": obj.version_id})

            if len(batch) == 1000:
                print("deleting %d objects from %s" % (len(batch), bucket.name))
                e.submit(bucket.delete_objects, Delete={"Objects": batch})
                batch = []

        if batch:
            print("deleting %d objects from %s" % (len(batch), bucket.name))
            e.submit(bucket.delete_objects, Delete={"Objects": batch})


def destroy_cluster(state, role, image):
    """Destroys a cluster."""

    # Terraform cannot remove S3 buckets that have objects. So we empty
    # the S3 buckets as a one-off.
    for bucket in find_cluster_s3_buckets(state):
        remove_s3_bucket_objects(bucket)

    with launch_bootstrap_ec2_instance(state, role, image) as instance:
        print("waiting for SSH connection...")
        ssh = wait_for_ssh(
            instance.private_ip_address, 22, timeout=60.0, username="root"
        )
        print("got SSH connection to bootstrap EC2 instance")
        print("to connect: $ ssh -l root %s" % instance.private_ip_address)
        with ssh:
            write_bootstrap_files(state, ssh)
            res = run_command_ssh(
                ssh, "/usr/bin/bash /bootstrap/bootstrap_from_instance.sh destroy"
            )

            if res:
                raise Exception("Error running Terraform")

    # And nuke the bootstrap IAM role, managed by us.
    delete_bootstrap_iam_role(state["aws_session"], state["cluster_region"], role.name)

    # We also nuke out files from the cluster state bucket, as they
    # shouldn't be necessary any more. And presence can interfere
    # with a new cluster under the same name.
    s3 = state["aws_session"].resource(
        "s3", region_name=state["cluster_state_bucket_region"]
    )
    bucket = s3.Bucket(state["cluster_state_bucket"])
    remove_s3_bucket_objects(bucket, "%s/" % state["cluster_state_bucket_key_prefix"])


# Add files in this list.
SUPPORT_FILES = [
    "requirements.txt",
    "ssh_authorized_keys",
    "bootstrap_instance_user_data.sh",
]

# Add directories in this list, recursively.
SUPPORT_DIRS = [
    "ansible",
    "templates",
    "terraform_modules",
]


def create_support_files_zip():
    """Create zip file data containing support files.

    This function assumes we are running from a source checkout.
    """
    b = io.BytesIO()

    with zipfile.ZipFile(b, "w") as zf:
        for f in SUPPORT_FILES:
            zf.write(HERE / f, f)

        for d in SUPPORT_DIRS:
            for root, dirs, files in os.walk(HERE / d):
                for f in files:
                    full = pathlib.Path(root) / f
                    zf.write(full, full.relative_to(HERE))

    return b.getvalue()


def create_self_contained_script():
    """Obtain a self-contained version of this file."""

    data = create_support_files_zip()
    data = base64.b64encode(data)

    support_data_lines = [
        'SUPPORT_DATA = r"""',
    ]

    for line in textwrap.wrap(data.decode("ascii")):
        support_data_lines.append(line)

    support_data_lines.append('""".strip()')

    lines = []
    marker_found = False

    with open(__file__, "r", encoding="utf-8") as fh:
        for line in fh:
            lines.append(line.rstrip())
            if lines[-1] == "# SUPPORT DATA MARKER":
                lines.extend(support_data_lines)
                marker_found = True

    if not marker_found:
        raise Exception('"SUPPORT_DATA_MARKER" line marker not found')

    return "\n".join(lines)


# The next line is used by assemble.py to insert the SUPPORT_DATA
# variable here, before we hit the __main__ block.
# SUPPORT DATA MARKER


def load_support_data():
    """Parses the inline zip file in the SUPPORT_DATA variable."""
    try:
        data = SUPPORT_DATA
    except NameError:
        raise Exception(
            "SUPPORT_DATA not found; you probably are not running a script produced with assemble.py..."
        )

    # Original data is base64 encoded with line breaks to avoid
    # long lines.
    data = data.replace("\n", "")
    data = base64.b64decode(data)

    files = {}

    with zipfile.ZipFile(io.BytesIO(data), "r") as zf:
        for zi in zf.infolist():
            with zf.open(zi.filename) as fh:
                files[zi.filename] = fh.read()

    return files


def bootstrap_python():
    """Called to bootstrap the Python environment.

    Creates / updates a virtualenv and reinvokes the current process.
    """
    support_files = load_support_data()

    venv_created = not VENV.exists()
    if venv_created:
        venv.create(VENV, with_pip=True)

    venv_requirements = VENV / "requirements.txt"
    with venv_requirements.open("wb") as fh:
        fh.write(support_files["requirements.txt"])
        fh.write(b"\n")

    python_path = VENV / "bin" / "python3"

    args = [
        str(python_path),
        "-m",
        "pip",
        "--disable-pip-version-check",
        "install",
        "-r",
        str(venv_requirements),
    ]

    if not venv_created:
        args.append("-q")

    print("activating virtualenv...")
    subprocess.run(args, check=True)

    os.environ["GITLAB_BOOTSTRAP_BOOTSTRAPPED"] = "1"
    os.environ["PATH"] = "%s%s%s" % (VENV / "bin", os.pathsep, os.environ["PATH"])

    print("respawning inside virtualenv...")
    subprocess.run([str(python_path), __file__] + sys.argv[1:], check=True)


def bootstrap_and_run_main():
    # This file relies on various other support files. In
    # the canonical repository, these files are separate files
    # to make development easier. To support a single file
    # "deployment," we produce a version of this file with inline
    # zip file data stored in the SUPPORT_DATA variable.
    #
    # Here, we try to load the support files. If the inline
    # variable isn't present, we try to produce a file with
    # the inline support data and run that.
    try:
        SUPPORT_DATA
    except NameError:
        # Not running a single file distribution of this script.
        # Are we running from source control?
        if not (HERE / "README.md").exists():
            print("support files not available; how did you get this file?")
            return 1

        # Cool, let's package up a new version of ourselves and
        # run that.
        #
        # We /could/ produce the zip data and inject it into
        # the appropriate global variable instead of executing
        # a whole Python process. The main reason we execute
        # the new file is for added testing that single-file
        # scripts work as expected.

        print("assembling single file bootstrap.py")

        new_script = create_self_contained_script()

        with tempfile.TemporaryDirectory(prefix="gitlab-bootstrap-") as td:
            bootstrap_path = pathlib.Path(td) / "bootstrap.py"

            with bootstrap_path.open("w", encoding="utf-8") as fh:
                fh.write(new_script)

            print("re-running with single file bootstrap.py")
            return subprocess.run(
                [sys.executable, str(bootstrap_path)] + sys.argv[1:]
            ).returncode

    # If we got here we must be running from a single file version
    # of this script. Let's proceed to activate a Python virtualenv.

    try:
        if "GITLAB_BOOTSTRAP_BOOTSTRAPPED" not in os.environ:
            bootstrap_python()
        else:
            return main()

    except subprocess.CalledProcessError as e:
        return e.returncode


if __name__ == "__main__":
    try:
        sys.exit(bootstrap_and_run_main())
    except KeyboardInterrupt:
        sys.exit(1)
