#!/bin/bash
# Shell script the defines the bootstrap EC2 instance user data.
# It is executed when the instance starts.
#
# This file is evaluated via jinja2 templating.

set -ex

# Allow dynamic list of SSH keys to be granted access.
cat > /root/.ssh/authorized_keys << EOF
{{ ssh_authorized_keys }}
EOF

sleep 1800
shutdown -h now
